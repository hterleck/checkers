package controller;

import view.BoardView;
import model.BoardModel;
import model.CheckerColor;
import model.MoveState;
import view.DialogBox;
import view.MainMenuView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingUtilities;

/**
 * Game board controller. Links together board view and board model and controls them.
 * Controlls user input during the game and reacts to it..
 * 
 * @author Szymon Andrzejuk
 */
public class BoardController {
	/** reference to board view */
	protected BoardView boardView;
	/** reference to board model */
	protected BoardModel boardModel;
	/** indicates move state */
	protected MoveState valid;
	/** indicates previous move state */
	protected MoveState prevValid;
	
	/**
	 * Empty class constructor. Used for creating board controller's subclass objects
	 */
	BoardController() {
		
	}

	/** 
	 * Class constructor
	 * 
	 * @param boardView reference to board view
	 * @param boardModel reference to board model
	 * @see BoardView
	 * @see BoardModel
	 */
	public BoardController(BoardView boardView, BoardModel boardModel) {
		this.boardView = boardView;
		this.boardModel  = boardModel;
		this.boardView.addCheckerDraggedListener(new CheckerDraggedListener());
		this.boardView.addCheckerReleasedListener(new CheckerReleasedListener());
		this.boardView.addEndGameButtonListener(new EndGameButtonListener());
		this.boardView.addGameRulesButtonListener(new GameRulesButtonListener());	
	}

    /**
	 * Gets board model
	 * @return reference to board model
	 */   
	public BoardModel getBoardModel() {
		return boardModel;
	}

	/**
	 * Catches mouse dragged on board view event. Provides an ability to move checkers around the screen
	 */
	private class CheckerDraggedListener implements MouseMotionListener {
		/**
		* Catches mouse dragged on board view event
		* 
		* @param e mouse motion event
		*/
	    @Override
	    public void mouseDragged(MouseEvent e) {
			/** Checks if user selected a checker and then "glues" that checker to mouse cursor until mouse button is released */
			if(boardView.isBeingDragged() == false && boardView.isMousePressed() == false) {
				int x = e.getX();
				int y = e.getY();
				dragChecker(x, y);
			}
			/** if checker is dragged - update it's position and repaint board */
			else if(boardView.getDraggedChecker() != null) {
				boardView.getCheckerDraggedPositionOnScreen().setLocation(e.getX() - boardView.getCheckerSize().getX()/2, 
					e.getY() - boardView.getCheckerSize().getY()/2);
				boardView.repaint();
			}
	    }

	    @Override
	    public void mouseMoved(MouseEvent e) {
			
	    }
	}
	
	/** 
	 * "glues" checker to mouse cursor if possible
	 * 
	 * @param x mouse cursor's x position
	 * @param y mouse cursor's y position
	 */ 
	protected void dragChecker(int x, int y) {
		boardView.setMousePressed(true);
		int ex = x;
		int ey = y;
		/** Take into account board's border sizes when calculating mouse cursor position */
		ex -= (int)boardView.getBorderWidth();
		ey -= (int)boardView.getBorderHeight();
		int px = -1, py = -1;
		double fieldWidth = boardView.getBoardSize().getX() / 8;
		double fieldHeight = boardView.getBoardSize().getY()/ 8;
		/** translates on screen cursor coordinates to on board coordinates */
		while(x >= 0 && px < 7) {
			x -= fieldWidth;
			++px;
		}
		while(y >= 0 && py < 7) {
			y -= fieldHeight;
			++py;
		}
		if(boardView.isMultijump()) {
			/** Only allows dragging already dragged checker when multijump occurs */
			if(boardModel.getChecker(py * 8 + px) == boardView.getDraggedChecker()) {
				boardView.setMultijump(false);
				boardView.setInitialCheckerPositionOnBoard(px, py);//.setLocation(px, py);
				boardView.setCheckerDraggedPositionOnScreen((int)(ex - boardView.getCheckerSize().getX()/2), 
					(int)(ey - boardView.getCheckerSize().getY()/2));
				boardView.repaint();
			}
			else
				return;
		}
		/** Checks if user selected correct checker */
		if(boardModel.getChecker(py * 8 + px) != null && boardModel.canColorMove(boardModel.getChecker(py * 8 + px))) {
			boardView.setDraggedChecker(boardModel.getChecker(py * 8 + px));
			boardView.setDragged(true);
			boardView.setInitialCheckerPositionOnBoard(px, py);
			boardView.setCheckerDraggedPositionOnScreen((int)(ex - boardView.getCheckerSize().getX()/2), 
				(int)(ey - boardView.getCheckerSize().getY()/2));
			boardView.repaint();
		}
	}

	/**
	 * Catches mouse released on board view event. When mouse button is released board model method is called to check
	 * if move made by user is correct and if is it then executes the move and updates board view data.
	 */
	private class CheckerReleasedListener implements MouseListener{

	    @Override
	    public void mouseClicked(MouseEvent e) {
		
	    }

	    @Override
	    public void mousePressed(MouseEvent e) {
		
	    }

		/**
		* Catches mouse released on board view event.
		* 
		* @param e mouse event
		*/
	    @Override
	    public void mouseReleased(MouseEvent e) {
			boardView.setMousePressed(false);
			/** if checker was dragged - drop it on board */
			if(boardView.isBeingDragged()) {
				boardView.setDragged(false);
				/** Take into account board's border sizes when calculating mouse cursor position */
				int x = e.getX() - (int)boardView.getBorderWidth();
				int y = e.getY() + (int)boardView.getBorderHeight();
				double fieldWidth = boardView.getFieldSize().getX();
				double fieldHeight = boardView.getFieldSize().getX();
				int px = -1, py = -1;
				while(x >= 0) {
					x -= fieldWidth;
					++px;
				}
				while(y >= 0) {
					y -= fieldHeight;
					++py;
				}
				/** Checks if mouse button was released over a board field */
				if(px >=0 && py >= 0 && px < 8 && py < 8) {
					/** checks if checker move can be made */
					valid = boardModel.moveValidation(boardView.getDraggedChecker(), py * 8 + px);
					System.out.println(valid);
					/** Prevents unlocking ability to move all checkers, when during multijump checker is dragged in incorrect position */
					if(valid == MoveState.INCORRECT && prevValid == MoveState.MULTIJUMP) {
						boardView.setMultijump(true);
						boardView.repaint();
						return;
					}
					
					if(valid == MoveState.CORRECT) {
						prevValid = MoveState.CORRECT;
					}
					/** Displays dialog with information for user that he has to jump */
					else if(valid == MoveState.DOJUMP) {
						DialogBox.jumpPossibleDialog(boardView.getFrame());
						boardView.setDraggedChecker(null);
						boardView.repaint();
						return;
					}
					/** if multijump situation - no need to reload game info */
					else if(valid == MoveState.MULTIJUMP) {
						boardView.setMultijump(true);
						prevValid = MoveState.MULTIJUMP;
						boardView.repaint();
						return;
					}
					/** reloads game info panel (current turn) */
					if(boardModel.getActualColor() == CheckerColor.WHITE) {
						boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().whiteCheckerImage);
						boardView.reloadGameInfoArea();
					}
					else {
						boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().blackCheckerImage);
						boardView.reloadGameInfoArea();
					}			
					boardView.setDraggedChecker(null);
					boardView.repaint();
                                        
                    if(boardModel.isOver()) {
						showEndGameStats();
					}
				}
				/** if checker wasn't dropped over a board field - set it to previous position */
				else {
					boardModel.setChecker(boardView.getDraggedChecker(), 
						boardView.getInitialCheckerPositionOnBoard().y * 8 + boardView.getInitialCheckerPositionOnBoard().x);
					boardView.setCheckerDraggedPositionOnBoard(boardView.getInitialCheckerPositionOnBoard().x, 
						boardView.getInitialCheckerPositionOnBoard().y);
					boardView.setDraggedChecker(null);
					boardView.repaint();
				}
			}
	    }

	    @Override
	    public void mouseEntered(MouseEvent e) {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	    }

	    @Override
	    public void mouseExited(MouseEvent e) {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	    }
	}
	
	/**
	 * Listens if "Back to menu" button was pressed during the game. When the button is pressed dialog box appears
	 * and asks user if he wants to end the game and go back to menu
	 */
	protected class EndGameButtonListener implements ActionListener {
        /**
         * Quits game after button action
         * 
         * @param e button event
         */
	    @Override
	    public void actionPerformed(ActionEvent e) {
			int n = DialogBox.showEndGameDialog(boardView.getFrame());
			if(n == 0) {
				boardView.getFrame().getContentPane().removeAll();
				backToMainMenu();
			}
	    }	
	}
	
	/** 
	 * Ends the game and goes back to main menu
	 */
    void backToMainMenu() {
		boardModel = null;
		boardView.getFrame().setSize(boardView.getFrame().getHeight(), boardView.getFrame().getHeight());
		boardView.getFrame().getContentPane().removeAll();	
		MainMenuView mainMenuView = new MainMenuView(boardView.getFrame(), boardView.getGraphicsAssets());
		MainMenuController c = new MainMenuController(mainMenuView);
		boardView.getFrame().add(mainMenuView);
		boardView.getFrame().validate();
		boardView.getFrame().repaint();
	}

	/**
	 * Listens if "Rules" button was pressed during the game. When the button is pressed dialog box appears
	 * and displays game rules
	 */
	protected class GameRulesButtonListener implements ActionListener {
        /**
         * Shows rules dialog when proper button is clicked
         * @param e button action event
         */
	    @Override
	    public void actionPerformed(ActionEvent e) {
		    DialogBox.showRulesDialog(boardView.getFrame());
	    }	
	}
	
	/** 
	 * Reloads game info area 
	 */
	protected void reload() {
		boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().whiteCheckerImage);
		boardView.reloadGameInfoArea();
		boardView.repaint();
	}
	
	/**
	 * Displays end game stats in dialog box(winner, game duration in minutes). Also offers ability to immediately restart the game
	 */
	private void showEndGameStats() {
		String winner = null;
		if(boardModel.getPlayer(0).isVictorious())
			winner = boardModel.getPlayer(0).getName();
		else if(boardModel.getPlayer(1).isVictorious())
			winner = boardModel.getPlayer(1).getName();
		int n = DialogBox.showEndGameStats(winner, System.currentTimeMillis() - boardModel.getStartTime(), boardView.getFrame());
		if(n == 0 || n == -1) {
			backToMainMenu();
			return;
		}
		boardModel.reinitializeBoard();
		boardView.reset();
		boardView.repaint();
		reload();
	}
}

