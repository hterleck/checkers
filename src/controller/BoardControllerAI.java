
package controller;

import model.BoardModel;
import model.CheckerColor;
import model.Move;
import model.MoveState;
import view.BoardView;
import view.DialogBox;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;

/**
 * Game board controller used in singleplayer mode. Links together board view and board model and controls them.
 * Controlls user input during the game and reacts to it.
 * 
 * @author Szymon Andrzejuk
 */
public class BoardControllerAI extends BoardController implements ActionListener {
	/** Singleplayer game difficulty (2 - easy, 5 - medium, 8 - hard) */
	private int gameDifficulty;
	/** flag indicating whose turn it is (false - user's turn, true - AI's) */
	private boolean aiTurn = false;
	/** color chosen by player */
	private CheckerColor color;
	/** game restart flag */
	private boolean gameRestart = false;
	
	/**
	 * Class constructor. Registers component listeners in board view
	 * @param boardView reference to board view
	 * @param boardModel reference to board model
	 * @param difficulty game difficulty (2 - easy, 5 - medium, 8 - hard)
	 * @param color checker's color chosen by the player
	 * @see BoardView
	 * @see BoardModel
	 * @see CheckerColor
	 */
	BoardControllerAI(BoardView boardView, BoardModel boardModel, int difficulty, CheckerColor color) {
		this.boardView = boardView;
		this.boardModel  = boardModel;
		
		this.boardView.addCheckerDraggedListener(new CheckerDraggedListener());
		this.boardView.addCheckerReleasedListener(new CheckerReleasedListener());
		this.boardView.addEndGameButtonListener(new EndGameButtonListener());
		this.boardView.addGameRulesButtonListener(new GameRulesButtonListener());
		
		gameDifficulty = difficulty;
		this.color = color;
		if(color == CheckerColor.WHITE) {
			aiTurn = false;
		}
		else {
			aiTurn = true;
			new Thread(new AIMove()).start();
		}		
	}

	/**
	 * Catches mouse dragged on board view event. Provides an ability to move checkers around the screen
	 */
	class CheckerDraggedListener implements MouseMotionListener {
		/**
		* Catches mouse dragged on board view event
		* 
		* @param e mouse motion event
		*/
	    @Override
	    public void mouseDragged(MouseEvent e) {
			/** Checks if user selected a checker and then "glues" that checker to mouse cursor until mouse button is released */
			if(boardView.isBeingDragged() == false && boardView.isMousePressed() == false && 
					boardModel.getActualColor() == color && aiTurn == false) {
				int x = e.getX();
				int y = e.getY();
				/**
				 * @see #dragChecker(controller.BoardController)
				 */
				dragChecker(x, y);
			}
			/** if checker is dragged - update it's position and repaint board */
			else if(boardView.getDraggedChecker() != null) {
				boardView.getCheckerDraggedPositionOnScreen().setLocation(e.getX() - boardView.getCheckerSize().getX()/2, 
					e.getY() - boardView.getCheckerSize().getY()/2);
				boardView.repaint();
			}	
	    }

		@Override
		public void mouseMoved(MouseEvent e) {

		}
	}
	
	/**
	 * Catches mouse released on board view event. When mouse button is released board model method is called to check
	 * if move made by user is correct and if is it then executes the move and updates board view data. 
	 * <p>After user's move invokes AI move.
	 */
	private class CheckerReleasedListener implements MouseListener{

	    @Override
	    public void mouseClicked(MouseEvent e) {
		
	    }

	    @Override
	    public void mousePressed(MouseEvent e) {
		
	    }

		/**
		* Catches mouse released on board view event.
		* 
		* @param e mouse event
		*/
	    @Override
	    public void mouseReleased(MouseEvent e) {
			boardView.setMousePressed(false);
			/** if checker was dragged - drop it on board */
			if(boardView.isBeingDragged()) {
				boardView.setDragged(false);
				/** Take into account board's border sizes when calculating mouse cursor position */
				int x = e.getX() - (int)boardView.getBorderWidth();
				int y = e.getY() + (int)boardView.getBorderHeight();
				double fieldWidth = boardView.getFieldSize().getX();
				double fieldHeight = boardView.getFieldSize().getX();
				int px = -1, py = -1;
				while(x >= 0) {
					x -= fieldWidth;
					++px;
				}
				while(y >= 0) {
					y -= fieldHeight;
					++py;
				}
				/** Checks if mouse button was released over a board field */
				if(px >=0 && py >= 0 && px < 8 && py < 8) {
					System.out.println("==== Player Move ====");//debug info
					valid = boardModel.moveValidation(boardView.getDraggedChecker(), py * 8 + px);
					System.out.println("Move state: " + valid);//debug info
					/** Prevents unlocking ability to move all checkers, when during multijump checker is dragged in incorrect position */
					if(valid == MoveState.INCORRECT && prevValid == MoveState.MULTIJUMP) {
						boardView.setMultijump(true);
						boardView.repaint();
						return;
					}
					
					if(valid == MoveState.CORRECT) {
						prevValid = MoveState.CORRECT;
					}
					/** Displays dialog with information for user that he has to jump */
					else if(valid == MoveState.DOJUMP) {
						DialogBox.jumpPossibleDialog(boardView.getFrame());
						boardView.setDraggedChecker(null);
						boardView.repaint();
						return;
					}
					/** if multijump situation - no need to reload game info */
					else if(valid == MoveState.MULTIJUMP) {
						boardView.setMultijump(true);
						prevValid = MoveState.MULTIJUMP;
						boardView.repaint();
						return;
					}
					/** reloads game info panel (current turn) */
					if(boardModel.getActualColor() == CheckerColor.WHITE) {
						boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().whiteCheckerImage);
						boardView.reloadGameInfoArea();
					}
					else {
						boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().blackCheckerImage);
						boardView.reloadGameInfoArea();
					}
                                        
					boardView.setDraggedChecker(null);
					boardView.repaint();
                                        
                    if(boardModel.isOver()) {
						showEndGameStats();
					}
                        
					/** If user's turn is over - invoke AI move */
					if(boardModel.getActualColor() == (color == CheckerColor.BLACK ? CheckerColor.WHITE : CheckerColor.BLACK)) {
						aiTurn = true;
						new Thread(new AIMove()).start();
					}
				}
				/** if checker wasn't released over a board field - set it to previous position */
				else {
					boardModel.setChecker(boardView.getDraggedChecker(), 
						boardView.getInitialCheckerPositionOnBoard().y * 8 + boardView.getInitialCheckerPositionOnBoard().x);
					boardView.setCheckerDraggedPositionOnBoard(boardView.getInitialCheckerPositionOnBoard().x, 
						boardView.getInitialCheckerPositionOnBoard().y);
					boardView.setDraggedChecker(null);
					boardView.repaint();
				}		
			}
	    }

	    @Override
	    public void mouseEntered(MouseEvent e) {
		
	    }

	    @Override
	    public void mouseExited(MouseEvent e) {
		
	    }
	}
	
	/**
	 * AI move and move animations.
	 */
	private class AIMove implements Runnable, ActionListener {
		AIMove() {}
		@Override
		public void run() {
			aiTurn = true;
			/** set up timer to reload game info properly after restart */
			if(gameRestart == true) {
				Timer timer = new Timer(0, this);
				timer.setRepeats(false);
				timer.start(); 
				gameRestart = false;
			}
			/** Synchronized in order to repaint board properly */
				boardView.repaint();
                        
			System.out.println("==== Computer Move ====");//debug info
			long time = System.currentTimeMillis();
			Move moves = boardModel.cpuMove((color == CheckerColor.BLACK ? CheckerColor.WHITE : CheckerColor.BLACK), gameDifficulty);
			if(moves == null) {
                System.out.println("Computer is blocked ? why here?!?");//debug info
				showEndGameStats();
				return;
			}
			/** put thread to sleep for minimum 1 second before displaying first cpu move */
			time = System.currentTimeMillis() - time;
			if(time < 1000)
			{
				try {
					Thread.sleep(1000 - time);
				} catch (InterruptedException ex) {
					Logger.getLogger(BoardControllerAI.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
                
			/** Moves checkers one at a time in 1 second intervals to create move animation */
			for(Iterator<BoardModel> it = moves.getBoards().iterator(); it.hasNext();) {
				boardModel = it.next();
				boardView.setModel(boardModel);
				boardView.repaint();
				try {
					if(it.hasNext()) {

						Thread.sleep(1000);
					}

				} catch (InterruptedException ex) {
					Logger.getLogger(BoardControllerAI.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			aiTurn = false;
			if(boardModel.isOver()) {
				showEndGameStats();
				return;
			}
			/** Timer required to properly render game info. Otherwise text would be shifted */
			Timer timer = new Timer(0, this);
			timer.setRepeats(false);
			timer.start(); 
		}

		/** Updates game info panel */
		@Override
		public void actionPerformed(ActionEvent e) {
			if(boardModel.getActualColor() == CheckerColor.WHITE)
				boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().whiteCheckerImage);
			else
				boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().blackCheckerImage);
			boardView.reloadGameInfoArea();
			boardView.repaint();	
		}
	}
	
	/**
	 * Displays end game stats (winner, game duration in minutes) 
	 */
	private void showEndGameStats() {
		String winner = null;
		if(boardModel.getPlayer(0).isVictorious())
			winner = boardModel.getPlayer(0).getName();
		else if(boardModel.getPlayer(1).isVictorious())
			winner = boardModel.getPlayer(1).getName();
		int n = DialogBox.showEndGameStats(winner, System.currentTimeMillis() - boardModel.getStartTime(), boardView.getFrame());
		if(n == 0 || n == -1) {
			backToMainMenu();
			return;
		}
		boardModel.reinitializeBoard();
		boardView.reset();
		reload();
		if(color == CheckerColor.WHITE) {
			/** Again, timer required to properly render game info. Otherwise text would be shifted */
			Timer timer = new Timer(0, this);
			timer.setRepeats(false);
			timer.start();
			aiTurn = false;
		}
		else {
			aiTurn = true;
			gameRestart = true;
			new Thread(new AIMove()).start();
		}			
	}
	
	/**
     * Updates game info panel 
     * 
     * @param e timer action event
     */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(boardModel.getActualColor() == CheckerColor.WHITE)
			boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().whiteCheckerImage);
		else
			boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().blackCheckerImage);
		boardView.reloadGameInfoArea();
		boardView.repaint();	
	}
}
