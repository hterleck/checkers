
package controller;

import network.EndOfGameEvent;
import network.StartOfGameEvent;
import network.NetBoardEvent;
import network.Client;
import network.Server;
import network.PlayerLeftGameEvent;
import model.BoardModel;
import model.CheckerColor;
import model.MoveState;
import model.NetBoard;
import network.EndBoxEvent;
import network.MessageEvent;
import network.RestartGameEvent;
import view.BoardView;
import view.DialogBox;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.net.UnknownHostException;
import javax.swing.Timer;

/**
 * Game board controller used in internet multiplayer mode. Links together board view and board model and controls them.
 * Controlls user input during the game and reacts to it.
 * 
 * @author Szymon Andrzejuk
 */
public class BoardControllerOnline extends BoardController implements ActionListener {
	/** reference to game server */
    private Server server;
	/** reference to game client */
    private Client client;
	/** reference to chat controller */
    protected ChatController chatController;
	/** board model data sent over internet */
	private NetBoard send;

	/** 
	 * Class constructor. Creates game server and a client connected to that server on server's end
	 * 
	 * @param boardView reference to board view
	 * @param boardModel reference to board model
	 * @see BoardView
	 * @see BoardModel
	 */
    BoardControllerOnline(BoardView boardView, BoardModel boardModel){
		this.boardView = boardView;
		this.boardModel  = boardModel;
		
        this.boardView.addCheckerDraggedListener(new CheckerDraggedListener());
		this.boardView.addCheckerReleasedListener(new CheckerReleasedListener());
		this.boardView.addEndGameButtonListener(new EndGameButtonListener());
		this.boardView.addGameRulesButtonListener(new GameRulesButtonListener());

		server = null;
		
		server  = new Server();
			try {        
				server.start();   
		} catch (IOException ex) {
			 DialogBox.showInformationWindow("Error","Port 5000 is busy, please try again later.", boardView.getFrame());
                    backToMainMenu();
                    return;
		}
			client = new Client("127.0.0.1" ,5000, this);
		try {
			client.start();
		} 
		catch (UnknownHostException exception) {
			exception.printStackTrace();
		}
		catch (IOException exception) {
			exception.printStackTrace();
			return;
		}
		chatController = new ChatController(boardView, client, boardModel.getPlayer(0).getName());
		
		boardModel.setOnlineLock(true);
		chatController.updateChat("Info: Game sucessfully created.");
		chatController.updateChat("Info: Waiting for opponent."); 
		
		
    }
	
	/** 
	 * Class constructor. Creates game client on client's end 
	 * 
	 * @param boardView reference to board view
	 * @param boardModel reference to board model
	 * @param ip ip address to connect to
	 * @see BoardView
	 * @see BoardModel
	 */
	BoardControllerOnline(BoardView boardView, BoardModel boardModel, String ip, Client client) {
		this.boardView = boardView;
		this.boardModel  = boardModel;
		
		this.boardView.addCheckerDraggedListener(new CheckerDraggedListener());
		this.boardView.addCheckerReleasedListener(new CheckerReleasedListener());
		this.boardView.addEndGameButtonListener(new EndGameButtonListener());
		this.boardView.addGameRulesButtonListener(new GameRulesButtonListener());

		this.client = client;
		chatController = new ChatController(boardView, client, boardModel.getPlayer(1).getName());
		boardModel.setOnlineLock(true);
		client.processEvent(new StartOfGameEvent(boardModel.getPlayer(1).getName()));
		
		
	}
	
	/**
	 * Checks if there's program's server running locally
	 * 
	 * @return true - server is running locally
	 */
	public boolean isServer() {
		return server == null ? false : true;
	}
	
	/** 
	 * Gets chat controller reference 
	 * 
	 * @return reference to chat controller
	 */
	public ChatController getChatController() {
		return chatController;	
	}
	
	/** 
	 * Updates player's name in game info panel after opponent's name is obtained through network.
	 * 
	 * @param name connecting player name
	 * @param i tells the program from who the name comes from (i == 0 - it comes from client and game info area needs updating) 
	 */
	public void updateName(String name, int i) {
		System.out.println("Hello " + name);
		boardModel.getPlayer(i).setName(name);
		if(i == 0) {
			Timer timer = new Timer(0, this);
			timer.setRepeats(false);
			timer.start();
		}
			boardView.repaint();
		if(i == 1){
			boardModel.setOnlineLock(false);
			boardModel.resetStartTime();
		}
	}
   
    /** 
	 * Updates board data when it is received from opposite player
	 * 
	 * @param netBoard data sent over the internet
	 */
    public void updateBoard(NetBoard netBoard) {
		boardModel.updateBoard(netBoard);
		boardView.setModel(boardModel);
        boardView.repaint();
		/** Timer required to properly render game info. Otherwise text would be shifted */
		Timer timer = new Timer(0, this);
		timer.setRepeats(false);
		timer.start(); 
		if(boardModel.isOver()) {
            client.processEvent(new EndBoxEvent());
		}
    }
	
    /**
	 * Sends board to opposite player over the internet
	 */
	public void boardSend() {
		send = new NetBoard(boardModel);
		client.processEvent(new NetBoardEvent(send));
    } 

	/** 
	 * Timer action listener
	 * 
	 * @param e timer action event
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		/** Updates game info panel */
		if(boardModel.getActualColor() == CheckerColor.WHITE) {
			boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().whiteCheckerImage);
			//boardView.reloadGameInfoArea();
		}
		else {
			boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().blackCheckerImage);
			//boardView.reloadGameInfoArea();
		}
		//boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().whiteCheckerImage);
		boardView.reloadGameInfoArea();
		boardView.repaint();
	}
	
	/**
	 * Catches mouse dragged on board view event. Provides an ability to move checkers around the screen
	 */
	class CheckerDraggedListener implements MouseMotionListener {
		/**
		* Catches mouse dragged on board view event
		* 
		* @param e mouse motion event
		*/
	    @Override
	    public void mouseDragged(MouseEvent e) {
			
			if(boardView.isBeingDragged() == false && boardView.isMousePressed() == false && boardModel.getOnlineLock() == false) {
					int x = e.getX();
				int y = e.getY();
				/**
				 * @see #dragChecker(controller.BoardController)
				 */
				dragChecker(x, y);
			}
			/** if checker is dragged - update it's position and repaint board */
			else if(boardView.getDraggedChecker() != null) {
				boardView.getCheckerDraggedPositionOnScreen().setLocation(e.getX() - boardView.getCheckerSize().getX()/2, 
					e.getY() - boardView.getCheckerSize().getY()/2);
				boardView.repaint();
			}
	    }

	    @Override
	    public void mouseMoved(MouseEvent e) {
	
	    }
	}
	
	/**
	 * Catches mouse released on board view event. When mouse button is released board model method is called to check
	 * if move made by user is correct and if is it then executes the move, updates board view data and sends data to the other player
	 */
	private class CheckerReleasedListener implements MouseListener {

	    @Override
	    public void mouseClicked(MouseEvent e) {
		
	    }

	    @Override
	    public void mousePressed(MouseEvent e) {
		
	    }

		/**
		* Catches mouse released on board view event.
		* 
		* @param e mouse event
		*/
	    @Override
	    public void mouseReleased(MouseEvent e) {
			boardView.setMousePressed(false);
			/** if checker was dragged - drop it on board */
			if(boardView.isBeingDragged() && boardModel.getOnlineLock() == false) {
				boardView.setDragged(false);
				/** Take into account board's border sizes when calculating mouse cursor position */
				int x = e.getX() - (int)boardView.getBorderWidth();
				int y = e.getY() + (int)boardView.getBorderHeight();
				double fieldWidth = boardView.getFieldSize().getX();
				System.out.println(x);
				double fieldHeight = boardView.getFieldSize().getX();
				int px = -1, py = -1;
				while(x >= 0) {
					x -= fieldWidth;
					++px;
				}
				while(y >= 0) {
					y -= fieldHeight;
					++py;
				}
				/** Checks if mouse button was released over a board field */
				if(px >=0 && py >= 0 && px < 8 && py < 8) {
					/** checks if checker move can be made */
					valid = boardModel.moveValidation(boardView.getDraggedChecker(), py * 8 + px);
					
					/** Prevents unlocking ability to move all checkers, when during multijump checker is dragged in incorrect position */
					if(valid == MoveState.INCORRECT && prevValid == MoveState.MULTIJUMP) {
						boardView.setMultijump(true);
						boardView.repaint();
						return;
					}
					
					/* if move is correct - send updated board data to the opponent */
					if(valid == MoveState.CORRECT) {
						prevValid = MoveState.CORRECT;
						boardModel.setOnlineLock(false);
						boardSend();
						boardModel.setOnlineLock(true);
					}
					/** Displays dialog with information for user that he has to jump */
					else if(valid == MoveState.DOJUMP) {
						DialogBox.jumpPossibleDialog(boardView.getFrame());
						boardView.setDraggedChecker(null);
						boardView.repaint();
						return;
					}
					/** if multijump situation - send updated board data to the opponent */
					else if(valid == MoveState.MULTIJUMP) {
						boardView.setMultijump(true);
						prevValid = MoveState.MULTIJUMP;
						boardModel.setOnlineLock(true);
						boardSend();
						boardModel.setOnlineLock(false);
						boardView.repaint();
						return;
					}
					/** reloads game info panel (current turn) */	
					if(boardModel.getActualColor() == CheckerColor.WHITE) {
						boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().whiteCheckerImage);
						boardView.reloadGameInfoArea();
					}
					else {
						boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().blackCheckerImage);
						boardView.reloadGameInfoArea();
					}

					boardView.setDraggedChecker(null);
					boardView.repaint();
                                        
                    if(boardModel.isOver()) {
                        client.processEvent(new EndBoxEvent());
					}

				}
				/** if checker wasn't dropped over a board field - set it to previous position */
				else {
					boardModel.setChecker(boardView.getDraggedChecker(), 
						boardView.getInitialCheckerPositionOnBoard().y * 8 + boardView.getInitialCheckerPositionOnBoard().x);
					boardView.setCheckerDraggedPositionOnBoard(boardView.getInitialCheckerPositionOnBoard().x, 
						boardView.getInitialCheckerPositionOnBoard().y);
					boardView.setDraggedChecker(null);
					boardView.repaint();
				}				
			}
	    }

		@Override
		public void mouseEntered(MouseEvent e) {
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			
		}
	}  
	
	/** 
	 * Displays end game stats in a dialog box and sends end game notification to other player
	 */
	public void showEndGameStats() {
		String winner = null;
		if(boardModel == null) {
			return;
		}
		if(boardModel.getPlayer(0).isVictorious())
			winner = boardModel.getPlayer(0).getName();
		else if(boardModel.getPlayer(1).isVictorious())
			winner = boardModel.getPlayer(1).getName();
		int n = DialogBox.showEndGameStats(winner, System.currentTimeMillis() - boardModel.getStartTime(), boardView.getFrame());

		boardModel.setOnlineLock(true);
		if(n == 0 || n == -1){
			client.processEvent(new PlayerLeftGameEvent());
			if(server != null) try {
				server.stop();
			} catch (IOException ex) {
				/* Server is already closed */
			}
			backToMainMenu();
		}
		/** If player chooses to restart the game - send proper information to the opponent and display message in chat window */
        else if(isServer()) {
			getChatController().updateChat("Restarting the game... waiting for client");
			boardModel.reinitializeBoard();
			boardView.reset();
			boardView.repaint();
			reload();
			client.processEvent(new RestartGameEvent());
		}
		else {
			boardModel.setOnlineLock(true);
			getChatController().updateChat("Restarting the game... waiting for server");
			boardModel.reinitializeBoard();
			boardView.reset();
			boardView.repaint();
			reload();
			client.processEvent(new RestartGameEvent());
		}
	}
        
	/**
	 * Displays dialog box with error message
	 */
	public void lostConnection(){
        if(boardModel != null)
		DialogBox.showInformationWindow("Lost connection","Your opponent has left the game.", boardView.getFrame());
		backToMainMenu();
	}
		
	/**
	 * Listens if "Back to menu" button was pressed during the game. If one player decides to leave the game he sends
	 * notifications to other player informing him that he left
	 */
    protected class EndGameButtonListener implements ActionListener {
		/**
		 * Displays end game dialog and sends information to the opponent if game is ended
		 * 
		 * @param e button pressed action
		 */
	    @Override
	    public void actionPerformed(ActionEvent e) {
			int n = DialogBox.showEndGameDialog(boardView.getFrame());
			if(n == 0) {
				backToMainMenu();
				client.processEvent(new PlayerLeftGameEvent());
				client.processEvent(new EndOfGameEvent());
				try {
					if(server != null ) server.stop();
				} catch (IOException ex) {
					System.out.println("Cannnot stop server.");
				}                      
			}
	    }	
	}
	
	/** 
	 * Reloads game info area 
	 */
	@Override
	public void reload() {
		boardView.setCurrentTurnIcon(boardView.getGraphicsAssets().whiteCheckerImage);
		boardView.reloadGameInfoArea();
		boardView.repaint();
	}
}
