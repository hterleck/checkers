/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import network.Client;
import network.MessageEvent;
import view.BoardView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controlls chat panel displaying received messages and sending typed messages
 *
 * @author Szymon Andrzejuk
 */
public class ChatController  {
	/** reference to board view */
	private BoardView boardView;
	/** reference to network client */
	private Client client;
	/** player's local name */
	private String localName;
		
	/** 
	 * Class constructor
	 * 
	 * @param boardView reference to board view
	 * @param client reference to network client
	 * @param name player's name
	 */
	ChatController(BoardView boardView, Client client, String name) {
		this.boardView = boardView;
		this.client = client;
		this.localName = name;
		
		this.boardView.addChatListener(new ChatListener());
	}
	
	/** 
	 * Listens to chat events and sends typed in messages
	 */
	public class ChatListener implements ActionListener{
		
		/**
		 * Displays typed messages in chat and sends the to the opponent
		 * 
		 * @param e chat action event (message typed)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Chat");
			StringBuilder text = new StringBuilder();
			text.append("[");
			text.append(localName);
			text.append("]: ");
			text.append(boardView.getChatTextField().getText().trim());
			boardView.getChatTextField().setText("");
			String send = new String(text);
			System.out.println(send);
			client.processEvent(new MessageEvent(send));
			boardView.getChatTextArea().append(send + '\n');
			boardView.getChatTextField().selectAll();
			boardView.getChatTextField().setCaretPosition(boardView.getChatTextField().getDocument().getLength());
		}	
	} 
	
	/**
	 * Updates chat window when message is received
	 * 
	 * @param message message text
	 */
	public void updateChat(String message) {
		boardView.getChatTextArea().append(message+"\n");
		boardView.getChatTextField().selectAll();
		boardView.getChatTextField().setCaretPosition(boardView.getChatTextField().getDocument().getLength());
	}
}

