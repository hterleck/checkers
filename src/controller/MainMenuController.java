
package controller;

import view.MainMenuView;
import view.MultiPlayerMenuView;
import view.SinglePlayerMenuView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingUtilities;

/**
 * Main menu controller. Controls main menu view and reacts to events that occur in the view
 * 
 * @author Szymon Andrzejuk
 */
public class MainMenuController {
	/** reference to main menu view */
	private MainMenuView mainMenuView;
	
	/**
	 * Class constructor. Registers listeners in main menu view
	 * 
	 * @param mainMenuView reference to main menu view
	 */
	public MainMenuController(MainMenuView mainMenuView) {
		this.mainMenuView = mainMenuView;
		this.mainMenuView.addWindowResizeBoxListener(new WindowResizeBoxListener(
			mainMenuView.getWindowSizeBox(), mainMenuView.getFrame(), mainMenuView.getGraphicsAssets()));
		this.mainMenuView.addSinglePlayerButtonListener(new SinglePlayerButtonListener());
		this.mainMenuView.addMultiPlayerButtonListener(new MultiPlayerButtonListener());
		this.mainMenuView.addExitButtonListener(new ExitButtonListener());
	}
	
	/**
	 * Catches "single player" button clicked event and constructs single player menu
	 */
	class SinglePlayerButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Single");
			mainMenuView.getFrame().getContentPane().removeAll();	
			SinglePlayerMenuView singlePlayerMenuView = new SinglePlayerMenuView(mainMenuView.getFrame(), mainMenuView.getGraphicsAssets());
			SinglePlayerMenuController s = new SinglePlayerMenuController(singlePlayerMenuView);
			mainMenuView.getFrame().add(singlePlayerMenuView);
			mainMenuView.getFrame().validate();
			mainMenuView = null;
		}
	}
	
	/**
	 * Catches "multi player" button clicked event and constructs multi player menu
	 */
	class MultiPlayerButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			mainMenuView.getFrame().getContentPane().removeAll();	
			MultiPlayerMenuView multiPlayerMenuView = new MultiPlayerMenuView(mainMenuView.getFrame(), mainMenuView.getGraphicsAssets());
			MultiPlayerMenuController s = new MultiPlayerMenuController(multiPlayerMenuView);
			mainMenuView.getFrame().add(multiPlayerMenuView);
			mainMenuView.getFrame().validate();
			mainMenuView = null;
		}	
	}
	
	/**
	 * Catches "exit" button clicked event and exits the program
	 */
	class ExitButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);	
		}	
	}
}
