
package controller;

import model.BoardModel;
import model.GameMode;
import network.Client;
import view.BoardView;
import view.DialogBox;
import view.MainMenuView;
import view.MultiPlayerMenuView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Multiplayer menu controller. Controls multi player menu view and reacts to events that occur in the view
 * 
 * @author Szymon Andrzejuk
 */
public class MultiPlayerMenuController {
	/** reference to multiplayer menu view */
	private MultiPlayerMenuView multiPlayerMenuView;
	/** reference to client. Used when joining the internet game */
	Client client;
	
	/** Class constructor. Registers listeners in multiplayer menu view
	 * 
	 * @param multiPlayerMenuView reference to multiplayer menu view
	 */
	public MultiPlayerMenuController(MultiPlayerMenuView multiPlayerMenuView) {
		this.multiPlayerMenuView = multiPlayerMenuView;
		this.multiPlayerMenuView.addWindowResizeBoxListener(new WindowResizeBoxListener(
				multiPlayerMenuView.getWindowSizeBox(), multiPlayerMenuView.getFrame(), multiPlayerMenuView.getGraphicsAssets()));
		this.multiPlayerMenuView.addHotSeatButtonListener(new HotSeatButtonListener());
		this.multiPlayerMenuView.addCreateGameButtonListener(new CreateGameButtonListener());
		this.multiPlayerMenuView.addJoinGameButtonListener(new JoinGameButtonListener());
		this.multiPlayerMenuView.addBackButtonListener(new BackButtonListener());
	}
	
	/**
	 * Catches "hot seat" button clicked event and creates local multiplayer game
	 */
	class HotSeatButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			final String name1 = DialogBox.showPlayerNameDialog(1, multiPlayerMenuView.getFrame());
			if(name1 == null) return;
			final String name2 = DialogBox.showPlayerNameDialog(2, multiPlayerMenuView.getFrame());
			if(name2 == null) return;
			multiPlayerMenuView.getFrame().getContentPane().removeAll();              
			BoardModel boardModel = new BoardModel(name1, name2, GameMode.LOCAL_MULTIPLAYER);
			BoardView boardView = new BoardView(multiPlayerMenuView.getFrame(), multiPlayerMenuView.getGraphicsAssets(), boardModel);
			BoardController b = new BoardController(boardView, boardModel);
			multiPlayerMenuView = null;
		}
		
	}
	
	/**
	 * Catches "create game" button clicked event and creates internet multiplayer game
	 */
	class CreateGameButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			final String name = DialogBox.showPlayerNameDialog(3, multiPlayerMenuView.getFrame());
			if(name == null) 
				return;
			multiPlayerMenuView.getFrame().getContentPane().removeAll();  
			BoardModel boardModel = new BoardModel(name, "", GameMode.INTERNET_MULTIPLAYER);
			BoardView boardView = new BoardView(multiPlayerMenuView.getFrame(), multiPlayerMenuView.getGraphicsAssets(), boardModel);
			BoardControllerOnline b = new BoardControllerOnline(boardView, boardModel);
			multiPlayerMenuView = null;
		}	
	}
	
	/**
	 * Catches "join game" button clicked event and joins to an already existing game
	 */
	class JoinGameButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			final String name = DialogBox.showPlayerNameDialog(4, multiPlayerMenuView.getFrame());
			if(name == null) return;
			final String ip = DialogBox.showIPDialog(multiPlayerMenuView.getFrame());
			if(ip == null) return;
			
			client = new Client(ip ,5000, null);

			try {
				client.start();
			}
			catch (UnknownHostException exception)
			{
				DialogBox.showInformationWindow("Error","Invalid ip address. Host doesn't respond", multiPlayerMenuView.getFrame());
				return;
			}
			catch (IOException exception)
			{
				DialogBox.showInformationWindow("Error","Host refused connection", multiPlayerMenuView.getFrame());
				return;
			} 
			 
			multiPlayerMenuView.getFrame().getContentPane().removeAll();   
			BoardModel boardModel = new BoardModel("", name, GameMode.INTERNET_MULTIPLAYER);
			BoardView boardView = new BoardView(multiPlayerMenuView.getFrame(), multiPlayerMenuView.getGraphicsAssets(), boardModel); 
			BoardControllerOnline b = new BoardControllerOnline(boardView, boardModel, ip, client);     
			client.setBoardControllerOnline(b);
			multiPlayerMenuView = null;
		}	
	}

	/**
	 * Catches "Back" button clicked event and creates main menu
	 */
	class BackButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			multiPlayerMenuView.getFrame().getContentPane().removeAll();	
			MainMenuView mainMenuView = new MainMenuView(multiPlayerMenuView.getFrame(), multiPlayerMenuView.getGraphicsAssets());
			MainMenuController c = new MainMenuController(mainMenuView);
			multiPlayerMenuView.getFrame().add(mainMenuView);
			multiPlayerMenuView.getFrame().validate();
			multiPlayerMenuView = null;
		}
	}
}
