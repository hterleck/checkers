
package controller;

import model.BoardModel;
import model.CheckerColor;
import model.GameMode;
import view.BoardView;
import view.DialogBox;
import view.MainMenuView;
import view.SinglePlayerMenuView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Multiplayer menu controller. Controls multi player menu view and reacts to events that occur in the view
 * 
 * @author Szymon Andrzejuk
 */
public class SinglePlayerMenuController {
	/** singleplayer game difficulty (2 - easy, 5 - medium, 8 - hard) */
	private int gameDifficulty;
	/** reference to seingleplayer menu view */
	private SinglePlayerMenuView singlePlayerMenuView;
	
	/**
	 * Class constructor. Registers listeners in multiplayer menu view
	 * 
	 * @param singlePlayerMenuView reference to singleplayer menu view
	 */
	public SinglePlayerMenuController(SinglePlayerMenuView singlePlayerMenuView) {
		this.singlePlayerMenuView = singlePlayerMenuView;
		this.singlePlayerMenuView.addWindowResizeBoxListener(new WindowResizeBoxListener(
				singlePlayerMenuView.getWindowSizeBox(), singlePlayerMenuView.getFrame(), singlePlayerMenuView.getGraphicsAssets()));
		this.singlePlayerMenuView.addBeginGameButtonListener(new BeginGameButtonListener());
		this.singlePlayerMenuView.addDifficultyBoxListener(new DifficultyBoxListener());
		this.singlePlayerMenuView.addBackButtonListener(new BackButtonListener());
	}
	
	/** 
	 * Gets game difficulty
	 * @return game difficulty
	 */
	public int getDifficulty() {
		return gameDifficulty;
	}
	
	/**
	 * Catches "begin game" button clicked event and starts singlepleyer game
	 */
	class BeginGameButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			final String name = DialogBox.showPlayerNameDialog(0, singlePlayerMenuView.getFrame());
			if(name == null) 
				return;
			final CheckerColor color = DialogBox.showCheckerColorBox(singlePlayerMenuView.getFrame());
			if(color == null)
				return;
			singlePlayerMenuView.getFrame().getContentPane().removeAll();  
			BoardModel boardModel;
			if(color == CheckerColor.WHITE)
				boardModel = new BoardModel(name, "Computer", GameMode.SINGLEPLAYER);
			else
				boardModel = new BoardModel("Computer", name, GameMode.SINGLEPLAYER);
			BoardView boardView = new BoardView(singlePlayerMenuView.getFrame(),singlePlayerMenuView.getGraphicsAssets(), boardModel);
			BoardControllerAI b = new BoardControllerAI(boardView, boardModel, getDifficulty(),  color);
			singlePlayerMenuView = null;
		}		
	}
	
	/**
	 * Catches "choose difficulty" box select event and sets difficulty level
	 */
	class DifficultyBoxListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String r = singlePlayerMenuView.getDifficultyBox().getSelectedItem().toString();
			switch (r) {
				case "Difficulty:":
					singlePlayerMenuView.getBeginGameButton().setEnabled(false);
					return;
				case "Easy":
					singlePlayerMenuView.getBeginGameButton().setEnabled(true);
					gameDifficulty = 2;
					break;
				case "Medium":
					singlePlayerMenuView.getBeginGameButton().setEnabled(true);
					gameDifficulty = 5;
					break;
				default:
					singlePlayerMenuView.getBeginGameButton().setEnabled(true);
					gameDifficulty = 8;
					break;
			}
		}		
	}
	
	/**
	 * Catches "back" button clicked event and creates local multiplayer game
	 */
	class BackButtonListener implements ActionListener {
		@Override
			public void actionPerformed(ActionEvent e) {
				singlePlayerMenuView.getFrame().getContentPane().removeAll();
				MainMenuView mainMenuView = new MainMenuView(singlePlayerMenuView.getFrame(), singlePlayerMenuView.getGraphicsAssets());
				MainMenuController c = new MainMenuController(mainMenuView);
				singlePlayerMenuView.getFrame().add(mainMenuView);
				singlePlayerMenuView.getFrame().validate();
				singlePlayerMenuView = null;
		}
	}
}
