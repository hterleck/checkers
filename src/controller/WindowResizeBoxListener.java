package controller;

import view.Frame;
import view.ImagesLoader;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import org.imgscalr.Scalr;

/**
 * Window size selection box listener class
 *
 * @author Szymon Andrzejuk
 */
public class WindowResizeBoxListener implements ActionListener, Runnable {
	/** window size selection box */
	private JComboBox windowSize;
	/** main frame of the program */
	private Frame mainFrame;
	/** loaded image assets into the program */
	private ImagesLoader graphicsAssets;
	
	/**
	 * Class constructor 
	 * 
	 * @param windowSize window size selection box
	 * @param mainFrame loaded image assets into the program
	 * @param images loaded image assets into the program
	 */
	public WindowResizeBoxListener(JComboBox windowSize, Frame mainFrame, ImagesLoader images) {
		this.windowSize = windowSize;
		this.mainFrame = mainFrame;
		graphicsAssets = images;
	}
	
	/** 
	 * Catches event sent by window size selection box
	 * 
	 * @param e reveived box event
	 */
	@Override
		public void actionPerformed(ActionEvent e) {
			String r = windowSize.getSelectedItem().toString();
		switch (r) {
			case "75%":
				mainFrame.setSize((int)(mainFrame.getDefaultSize() * 0.75), (int)(mainFrame.getDefaultSize() * 0.75));
				break;
			case "100%":
				mainFrame.setSize((int)(mainFrame.getDefaultSize()), (int)(mainFrame.getDefaultSize()));
				break;
			case "125%":
				mainFrame.setSize((int)(mainFrame.getDefaultSize() * 1.25), (int)(mainFrame.getDefaultSize() * 1.25));
				break;
			case "150%":
				mainFrame.setSize((int)(mainFrame.getDefaultSize() * 1.5), (int)(mainFrame.getDefaultSize() * 1.5));
				break;
			case "175%":
				mainFrame.setSize((int)(mainFrame.getDefaultSize() * 1.75), (int)(mainFrame.getDefaultSize() * 1.75));
				break;
			case "200%":
				mainFrame.setSize((int)(mainFrame.getDefaultSize() * 2), (int)(mainFrame.getDefaultSize() * 2));
				break;			
		}
		System.out.println("window resized");
		windowSize.setSelectedIndex(0);
		mainFrame.validate();
		new Thread(this).start();		
	}

	/**
	 * Resizes backgroung menu image after program's window was resized
	 */
	@Override
	public void run() {
		graphicsAssets.scaledBackgroundMenuImage = Scalr.resize(graphicsAssets.backgroundMenuImage, 
			Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 
			(int)(mainFrame.getContentPane().getWidth()), 
			(int)(mainFrame.getContentPane().getHeight()), Scalr.OP_ANTIALIAS);
		System.out.println("repaint after resize");
	   mainFrame.repaint();
	}
}