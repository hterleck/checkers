package model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Artificial Intelligence class
 *
 * AI class contain only static variables and static methods
 *
 * @author Hubert Terlecki
 */
public class AI {

    /**
     * INFINITY declaration
     */
    public static final int INFINITY = 99999;
    /**
     * Search depth limit
     */
    private static int limit;
    /**
     * Actual level of searching color
     */
    private static CheckerColor actualColor;
    /**
     * Color for which we are looking for best move
     */
    private static CheckerColor searchColor;
    /* Maximum depth we reached*/
    private static int retDepth;

    /**
     * Method evaluates game state value
     *
     * @param tab reference to BoardModel for which we evaluate
     * @return evaluated value
     */
    private static int eval(BoardModel tab) {
        /* Variable holds value to return */
        int sum = 0;

        /* We iterate through all white checkers */
        for (Iterator<WhiteChecker> it = tab.getWhiteCheckersArray().iterator(); it.hasNext();) {
            Checker i = it.next();

            /* Evaluating */
            int line = i.getPositionY();
            sum += CheckerColor.WHITE == searchColor ? 1.5 * i.getValue() : i.getValue();
            if (line > 5) {
                sum -= 150;
            } else if (line == 2 || line == 3) {
                sum -= 50;
            } else if (line == 4 || line == 5) {
                sum -= 100;
            }
        }
        /* We iterate through all black checkers */
        for (Iterator<BlackChecker> it = tab.getBlackCheckersArray().iterator(); it.hasNext();) {
            Checker i = it.next();

            /* Evaluating */
            int line = i.getPositionY();
            sum += CheckerColor.BLACK == searchColor ? 1.5 * i.getValue() : i.getValue();
            if (line < 2) {
                sum += 150;
            } else if (line == 2 || line == 3) {
                sum += 100;
            } else if (line == 4 || line == 5) {
                sum += 50;
            }

        }
        /* Returns value positivie or negative accordingly to searchColor */
        return searchColor == CheckerColor.BLACK ? sum : -sum;

    }

    /**
     * Method initializes new tree search to find best move
     *
     * @param color color of player which should get best move
     * @param tab board of current game
     * @param _limit maximum depth which we can reach
     * @param d one element table holding the maximum depth we reached
     * @return best move value
     */
    static int newSearch(CheckerColor color, BoardModel tab, int _limit, int[] d) {
        /* Color initialization */
        actualColor = searchColor = color;
        /* Limit initialization */
        limit = _limit;

        /* Start searching and save its return value */
        int value = valFun(tab, 1, -INFINITY, INFINITY); // Wywolanie funckji genererujacej przyszleruchy
        /* Assign max reached depth */
        d[0] = retDepth;
        return value;
    }

    /**
     * Genereates new moves for minimax
     *
     * Checks if its end of searching and if it is returns board value if not
     * method generates new moves for minimax
     *
     * @param tab board to check and maybe generate new moves
     * @param depth actual tree depth
     * @param alpha actual alpha value for Alpha-Beta Cut
     * @param beta actual beta value for Alpha-Beta Cut
     * @return value of the gameboard
     */
    private static int valFun(BoardModel tab, int depth, int alpha, int beta) {
        /* Temporary vriable to hold score if game ends */
        int score;
        /* Finds whether we maximize or minimize on this depth */
        boolean minmax = depth % 2 == 1 ? true : false;
        ArrayList<Move> moves = new ArrayList<>();
        /* Check if its end */
        if ((score = win(tab)) != -1) {
            retDepth = depth;
            return score;
        }
        /* We reached maximum depth */
        if (depth >= limit) {
            retDepth = depth;
            /* Evaluate gameboard value */
            return eval(tab);
        }
        /* Set actual color */
        actualColor = (minmax == false) ? searchColor : searchColor.opposite();
        /* Find new moves list */
        findMoves(moves, tab);
        /* No more moves so its over */
        if (moves.isEmpty()) {
            retDepth = depth;
            return (minmax == false) ? -INFINITY : INFINITY;
        }
        /* maximize or minimize */
        if (minmax == true) {
            return minValue(moves, depth, alpha, beta);
        } else {
            return maxValue(moves, depth, alpha, beta);
        }
    }

    /**
     * Finds moves list for actualColor
     *
     * @param moves object to carry the moves
     * @param tab gameboard in which we are searching
     * @return true if it founds one or more moves, otherwise false
     */
    static boolean findMoves(ArrayList<Move> moves, BoardModel tab) {
        /* Variable holding return value */
        boolean ret = true;

        /* Choose color */
        if (actualColor == CheckerColor.WHITE) {
            if (tab.findJumps(CheckerColor.WHITE, moves) == false) // 
            {
                ret = tab.findNormalMoves(CheckerColor.WHITE, moves);
            }
        } else {
            if (tab.findJumps(CheckerColor.BLACK, moves) == false) // no jumps so we can move normally
            {
                ret = tab.findNormalMoves(CheckerColor.BLACK, moves);
            }
        }
        /* We need to delete marked not final moves*/
        for (int i = 0; i < moves.size(); i++) {
            if (moves.get(i).toDel == true) {
                moves.remove(i);
                i--;
            }
        }
        return ret;

    }

    /**
     * Setting actual color
     *
     * @param color set to AI.actualColor
     */
    static void setColor(CheckerColor color) {
        actualColor = color;
    }

    /**
     * Finds maximum value of move from list
     *
     * @param moves moves vector
     * @param depth depth of search tree
     * @param alpha alpha value for Alpha-Beta cut
     * @param beta beta value for Alpha-Beta cut
     * @return value of the best move
     */
    private static int maxValue(ArrayList<Move> moves, int depth, int alpha, int beta) // Znajdujemy najlepszy ruch dla MINa
    {
        /* The lowest value */
        int value = -INFINITY;

        for (Iterator<Move> it = moves.iterator(); it.hasNext();) {
            Move i = it.next();
            /* Go deeper and get better value */
            value = Math.max(value, valFun(i.boards.get(i.boards.size() - 1), depth + 1, alpha, beta));
            /* We can't reach best value */
            if (value >= beta) {
                return value;
            }
            /* New alpha value */
            alpha = Math.max(alpha, value);
        }
        return value;
    }

    /**
     * Finds minimum value of move from list
     *
     * @param moves moves vector
     * @param depth depth of search tree
     * @param alpha alpha value for Alpha-Beta cut
     * @param beta beta value for Alpha-Beta cut
     * @return value of the best move
     */
    private static int minValue(ArrayList<Move> moves, int depth, int alpha, int beta) // Znajdujemy najlepszy ruch dla MINa
    {
        /* Largest value */
        int value = INFINITY;

        for (Iterator<Move> it = moves.iterator(); it.hasNext();) {
            Move i = it.next();
            /* Go deeper and get better value */
            value = Math.min(value, valFun(i.boards.get(i.boards.size() - 1), depth + 1, alpha, beta));
            /* We can't get better value */
            if (value <= alpha) {
                return value;
            }
            /* New beta value*/
            beta = Math.min(beta, value);
        }
        return value;
    }

    /**
     * Check if its end of game
     *
     * @param tab gameboard to check
     * @return -1 when when its no over, -INFINITY minimum wins, INFINITY
     * maximum wins, 0 draw
     */
    private static int win(BoardModel tab) {
        boolean white = false;
        boolean black = false;

        if (tab.getBlackCheckersArray().isEmpty() == false) {
            black = true;
        }
        if (tab.getWhiteCheckersArray().isEmpty() == false) {
            white = true;
        }
        /* CAn we play longer */
        if (black == true && white == true) {
            return -1;
        }
        /* Draw */
        if (tab.getWhiteKingsMoves() >= 15 && tab.getBlackKingsMoves() >= 15) {
            return 0;
        }
        /* Somebody wins */
        if (searchColor == CheckerColor.BLACK) {
            if (black == false) {
                return -INFINITY;
            } else {
                return INFINITY;
            }
        } else {
            if (white == false) {
                return -INFINITY;
            } else {
                return INFINITY;
            }
        }
    }
}
