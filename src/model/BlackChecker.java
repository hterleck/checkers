package model;

/**
 * Black checker class
 *
 * @author Hubert Terlecki
 */
public class BlackChecker extends Checker {

    private static final long serialVersionUID = 1L;

    /**
     * BlackChecker deafult constructor
     */
    public BlackChecker() {
        super(CheckerColor.BLACK);
    }

    /**
     * BlackChecker constructor
     *
     * @param pos position on board
     * @param type type of checker
     */
    public BlackChecker(int pos, CheckerType type) {
        super(pos, type, CheckerColor.BLACK);
    }

    /**
     * BlackChecker copying constructor
     *
     * @param ch checker to copy
     */
    public BlackChecker(Checker ch) {
        super(ch);
    }
}