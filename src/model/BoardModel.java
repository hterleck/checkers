package model;

import static model.CheckerColor.BLACK;
import java.util.ArrayList;
import static model.CheckerColor.WHITE;
import static model.CheckerType.KING;
import static model.CheckerType.NORMAL;
import java.util.Iterator;
import java.util.Random;

/**
 * Game Board class
 *
 * Main class of model, contains phisycal structures, which build the board and
 * methodes that handle whole game logic
 *
 * @author Hubert Terlecki
 */
public class BoardModel {

    /*  64 Array of the fields on gameboard */
    private Field fields[];
    /*  Black checkers vector */
    private ArrayList<BlackChecker> blackCheckers;
    /* White checkers vector */
    private ArrayList<WhiteChecker> whiteCheckers;
    /*  Actual color to be played */
    private CheckerColor actualMoveColor = WHITE;
    /*  Number of kings moves for each color */
    private int whiteKingsMoves = 0;
    private int blackKingsMoves = 0;
    /* Array of players */
    private Player players[];
    /*  Game mode */
    private GameMode gameMode;
    /*  Start time of the game */
    private long startTime;
    /* Lock for online game */
    private boolean onlineLock;

    /**
     * Default constructor
     */
    private BoardModel() {
        fields = new Field[64];
        for (int i = 0; i < 64; ++i) {
            fields[i] = new Field();
        }
        blackCheckers = new ArrayList<>();
        whiteCheckers = new ArrayList<>();

        players = new Player[2];
    }

    /**
     * Not true copying constructor Copies everything except checkers and color,
     * which is given
     *
     * @param board BoardModel to be copied
     * @param color color of new board
     */
    public BoardModel(BoardModel board, CheckerColor color) {
        /* Call deafult c-tor */
        this();

        /* Copying checkers references */
        for (Iterator<WhiteChecker> it = board.getWhiteCheckersArray().iterator(); it.hasNext();) {
            WhiteChecker i = it.next();

            whiteCheckers.add(i);
            fields[i.getPositionOnBoard()].setChecker(i);
        }
        for (Iterator<BlackChecker> it = board.getBlackCheckersArray().iterator(); it.hasNext();) {
            BlackChecker i = it.next();

            blackCheckers.add(i);
            fields[i.getPositionOnBoard()].setChecker(i);
        }

        /* Coping player info */
        for (int i = 0; i < 2; ++i) {
            players[i] = new Player(board.getPlayer(i).getName());
            // players[i].setVictorious(board.getPlayer(i).isVictorious());
        }
        actualMoveColor = color;
        /* Copying other important values */
        whiteKingsMoves = board.getWhiteKingsMoves();
        blackKingsMoves = board.getBlackKingsMoves();
        gameMode = board.getGameMode();
        startTime = board.getStartTime();
        onlineLock = board.getOnlineLock();
    }

    /**
     * Game Starting constructor
     *
     * @param playerName1 palyer 1 name
     * @param playerName2 player 2 name
     * @param mode type of game
     */
    public BoardModel(String playerName1, String playerName2, GameMode mode) {
        /* Call deafult c-tor */
        this();
        /* Add checkers to board */
        addCheckers();
        //test();
        /* Creating players */
        players[0] = new Player(playerName1);
        players[1] = new Player(playerName2);

        startTime = System.currentTimeMillis();
        gameMode = mode;
    }

    /**
     * Returns checker color
     *
     * @return color of the checker
     */
    private CheckerColor getCheckerColor() {
        return actualMoveColor;
    }

    /**
     * Returns white kings moves number
     *
     * @return white kings moves number
     */
    int getWhiteKingsMoves() {
        return whiteKingsMoves;
    }

    /**
     * Returns black kings moves number
     *
     * @return black kings moves number
     */
    int getBlackKingsMoves() {
        return whiteKingsMoves;
    }

    /**
     * Reset game starting time
     */
    public void resetStartTime() {
        startTime = System.currentTimeMillis();
    }

    /**
     * Geneterate gameboard rom netboard
     *
     * @param netBoard
     */
    public void updateBoard(NetBoard netBoard) {
        /* Assign netBoard vectors to current gameboard */
        blackCheckers = netBoard.getBlackCheckersArray();
        whiteCheckers = netBoard.getWhiteCheckersArray();
        /* null fields of board */
        for (int i = 0; i < 64; ++i) {
            fields[i].setChecker(null);
        }
        /* Set checkers new properties*/
        for (Iterator<WhiteChecker> it = whiteCheckers.iterator(); it.hasNext();) {
            WhiteChecker i = it.next();
            fields[i.getPositionOnBoard()].setChecker(i);
        }

        for (Iterator<BlackChecker> it = blackCheckers.iterator(); it.hasNext();) {
            BlackChecker i = it.next();
            fields[i.getPositionOnBoard()].setChecker(i);
        }
        /* Copy other information */
        whiteKingsMoves = netBoard.getWhiteKingsMoves();
        blackKingsMoves = netBoard.getBlackKingsMoves();
        actualMoveColor = netBoard.getCheckerColor();
        onlineLock = netBoard.getOnlineLock();
    }

    /**
     * Reinitialize board states
     */
    public void reinitializeBoard() {
        for (int i = 0; i < 64; ++i) {
            fields[i].setChecker(null);
        }
        blackCheckers.clear();
        whiteCheckers.clear();
        addCheckers();
        actualMoveColor = WHITE;
        whiteKingsMoves = 0;
        blackKingsMoves = 0;
        players[0].setVictorious(false);
        players[1].setVictorious(false);
        startTime = System.currentTimeMillis();
    }

    /**
     * Returns information about gameboard lock
     *
     * @return true if board is locked
     */
    public boolean getOnlineLock() {
        return onlineLock;
    }

    /**
     * Sets information about gameboard lock
     *
     * @param val true if we want lock, otherwise false
     */
    public void setOnlineLock(boolean val) {
        onlineLock = val;
    }

    /**
     * Tells us if given checker can move now
     *
     * @param i Checker to check
     * @return true if can move, otherwise false
     */
    public boolean canColorMove(Checker i) {
        return (i.getColor() == actualMoveColor);
    }

    /**
     * Check is game is over and update players flags
     *
     * @return true if game is over, otherwise false
     */
    public boolean isOver() {
        /* Black lost */
        if (blackCheckers.isEmpty() || (actualMoveColor == BLACK && hasMove(BLACK) == false)) {
            players[0].setVictorious(true);
        }
        /* White lost */
        if (whiteCheckers.isEmpty() || (actualMoveColor == WHITE && hasMove(WHITE) == false)) {
            players[1].setVictorious(true);
        }
        /* Check if sb lost */
        if ((players[0].isVictorious() && players[1].isVictorious() == false)
                || (players[1].isVictorious() && players[0].isVictorious() == false)) {
            return true;
        }
        if (whiteKingsMoves >= 15 && blackKingsMoves >= 15) {
            return true;
        }
        return false;
    }

	/**
	 * Gets game mode (singleplayer/hot seat/multiplayer)
	 * @return game mode
	 */
    public GameMode getGameMode() {
        return gameMode;
    }

	/**
	 * Changes current checker color, when player's turn ends
	 */
    private void changeActualColor() {
        actualMoveColor = actualMoveColor == WHITE ? BLACK : WHITE;
    }

	/**
	 * Gets game start time
	 * 
	 * @return start time
	 */
    public long getStartTime() {
        return startTime;
    }

	/** 
	 * Gets player name
	 * 
	 * @param i player's number
	 * @return player's name
	 */
    public Player getPlayer(int i) {
        return players[i];
    }

	/**
	 * Gets actual turn checker color
	 * 
	 * @return checker color
	 */
    public CheckerColor getActualColor() {
        return actualMoveColor;
    }

    /**
     * Add checkers on start fields of boards
     */
    private void addCheckers() {
        int x = 0, y = 5;
        for (int i = 0; i < 12; ++i) {
            BlackChecker ch = new BlackChecker();
            ch.setPositionOnBoard(x, y);
            setChecker(ch, y * 8 + x);
            blackCheckers.add(ch);
            x += 2;
            if (x >= 8) {
                ++y;
                if (x % 2 == 0) {
                    x = 1;
                } else {
                    x = 0;
                }
            } // if(x >= 8)
        } // for
        x = 1;
        y = 0;
        for (int i = 0; i < 12; ++i) {
            WhiteChecker ch = new WhiteChecker();
            ch.setPositionOnBoard(x, y);
            setChecker(ch, y * 8 + x);
            whiteCheckers.add(ch);
            x += 2;
            if (x >= 8) {
                ++y;
                if (x % 2 == 0) {
                    x = 1;
                } else {
                    x = 0;
                }
            }// if(x >= 8)
        } // for
        // test();                
    } // private final void addCheckers()

	/**
	 * Gets white checkers array list
	 * 
	 * @return reference to white checkers array list
	 */
    public ArrayList<WhiteChecker> getWhiteCheckersArray() {
        return whiteCheckers;
    }

	/**
	 * Gets black checkers array list
	 * 
	 * @return reference to black checkers array list
	 */
    public ArrayList<BlackChecker> getBlackCheckersArray() {
        return blackCheckers;
    }

	/**
	 * Gets reference to checker standing on a specific field on board
	 * 
	 * @param n field number (0-63)
	 * @return reference to checker
	 */
    public Checker getChecker(int n) {
        return fields[n].getChecker();
    }
    /**
     * Reset kings move counter ( called after jump )
     */
    private void resetKingsMovesCounter() {
        whiteKingsMoves = 0;
        blackKingsMoves = 0;
    }
    /**
     * Sets checker on board field
     * 
     * @param checker checker tahat we want to set
     * @param n number of field on which we put checker
     */
    public void setChecker(Checker checker, int n) {
        fields[n].setChecker(checker);
    }

    /**
     * Delete checker form board
     *
     * @param ch checker to be deleted
     */
    private void deleteChecker(Checker ch) {
        fields[ch.getPositionOnBoard()].setChecker(null);
        switch (ch.getColor()) {
            case WHITE:
                whiteCheckers.remove((WhiteChecker) ch);
                break;
            case BLACK:
                blackCheckers.remove((BlackChecker) ch);
        }
    }

    /**
     * increment kings moves counter
     *
     * @param color color of moved king
     */
    private void incrementKingsMoves(CheckerColor color) {
        if (color == WHITE) {
            whiteKingsMoves++;
        } else {
            blackKingsMoves++;
        }
    }

    /**
     * Change board to move checker
     *
     * @param ch checker which was moved
     * @param position position after move
     * @return information about validation of move
     */
    private MoveState processHumanMove(Checker ch, int position) {
        if (hasJump(ch.getColor())) {
            return MoveState.DOJUMP;
        }
        fields[ch.getPositionOnBoard()].setChecker(null);
        ch.setPositionOnBoard(position % 8, position / 8);
        fields[position].setChecker(ch);
        changeActualColor();
        ch.promote();
        return MoveState.CORRECT;
    }

    /**
     * Change board to move checker while jumping
     *
     * @param ch checker which was moved
     * @param position position after move
     * @return information about validation of move
     */
    private MoveState processHumanJump(Checker ch, int position) {
        fields[ch.getPositionOnBoard()].setChecker(null);
        ch.setPositionOnBoard(position % 8, position / 8);
        fields[position].setChecker(ch);
        resetKingsMovesCounter();
        if (!hasJump(ch)) {
            changeActualColor();
            ch.promote();
            return MoveState.CORRECT;
        }
        return MoveState.MULTIJUMP;
    }

    /**
     * Validates players move on given checker
     *
     * @param ch checker, which should change position
     * @param pos new position of checker
     * @return Information about validation of move
     */
    public MoveState moveValidation(Checker ch, int pos) {
        /* Out of board */
        System.out.println("wKingsM: " + whiteKingsMoves + " bKingsMoves: " + blackKingsMoves);
        if (pos < 0 || pos > 63) {
            return MoveState.INCORRECT;
        }
        /* Check if checker is set and move is on diagonal */
        if (fields[pos].getChecker() != null || (Math.abs(pos % 8 - ch.getPositionX()) != Math.abs(pos / 8 - ch.getPositionY()))) {
            return MoveState.INCORRECT;
        }
        if (ch.getType() == NORMAL) {
            return validateNormalMove(ch, pos);
        } else {
            return validateKingMove(ch, pos);
        }
    }

    /**
     * Validates players move on given normal checker
     *
     * @param ch checker, which should change position
     * @param pos new position of checker
     * @return Information about validation of move
     */
    private MoveState validateNormalMove(Checker ch, int pos) {
        /* Some counting */
        int posX = ch.getPositionX();
        int posY = ch.getPositionY();
        int x = pos % 8;
        int y = pos / 8;
        Checker copy;
        CheckerColor color = ch.getColor();
        System.out.println("Position: " + pos + " x:" + x + " y:" + y);

        if (color == WHITE) {
            /* White checkers */
            if (y == posY + 1 && (x == posX + 1 || x == posX - 1)) {
                return processHumanMove(ch, pos);
            }
        } // if(color == BLACK)
        else {
            /* Black checkers */
            if (y == posY - 1 && (x == posX + 1 || x == posX - 1)) {
                return processHumanMove(ch, pos);
            } // if(y == posY - 1 ...
        } // else

        /* MAYBE WE CAN JUMP */
        if (y == posY + 2) {
            /* Player wanna jump down so check down left and right*/
            if (x == posX + 2 && fields[(posX + 1) + 8 * (posY + 1)].getChecker() != null
                    && (copy = fields[(posX + 1) + 8 * (posY + 1)].getChecker()).getColor() == color.opposite()) {
                //deleteChecker(posX + 1, posY + 1, color.opposite());
                deleteChecker(copy);
                return processHumanJump(ch, pos);
            } // if(x == posX + 2 ...
            if (x == posX - 2 && fields[(posX - 1) + 8 * (posY + 1)].getChecker() != null
                    && (copy = fields[(posX - 1) + 8 * (posY + 1)].getChecker()).getColor() == color.opposite()) {
                deleteChecker(copy);
                return processHumanJump(ch, pos);
            } // if(x == posX - 2 ...
        } // if(y == posY + 2 )
        else if (y == posY - 2) {
            /* Player wanna jump up so check up left and right*/
            if (x == posX + 2 && fields[(posX + 1) + 8 * (posY - 1)].getChecker() != null
                    && (copy = fields[(posX + 1) + 8 * (posY - 1)].getChecker()).getColor() == color.opposite()) {
                deleteChecker(copy);
                return processHumanJump(ch, pos);
            }// (x == posX + 2 &&
            if (x == posX - 2 && fields[(posX - 1) + 8 * (posY - 1)].getChecker() != null
                    && (copy = fields[(posX - 1) + 8 * (posY - 1)].getChecker()).getColor() == color.opposite()) {
                deleteChecker(copy);
                return processHumanJump(ch, pos);
            } // (x == posX - 2 && ...
        } // else if (y == posY - 2)
        return MoveState.INCORRECT;
    }

    /**
     * Validates players move on given king checker
     *
     * @param ch checker, which should change position
     * @param pos new position of checker
     * @return Information about validation of move
     */
    private MoveState validateKingMove(Checker ch, int pos) {
        /* Some counting */
        int posX = ch.getPositionX();
        int posY = ch.getPositionY();
        int x = pos % 8;
        int y = pos / 8;
        Checker copy, jumped = null;
        CheckerColor color = ch.getColor();
        System.out.println("Position: " + pos + " x:" + x + " y:" + y);

        /* We will be searching each direction in order */
        if (x > posX && y > posY) {
            for (int i = 1; i < 8; ++i) {
                if (posX + i < 8 && posY + i < 8) {
                    /* Down right */
                    if (x == posX + i && y == posY + i) {
                        if (jumped == null) {
                            /* Just move */
                            incrementKingsMoves(color);
                            return processHumanMove(ch, pos);
                        } // if(jumped == null)
                        else {
                            /* Jump */
                            deleteChecker(jumped);
                            return processHumanJump(ch, pos);
                        }// else

                    } // if(x == posX + i && y == posY + i)
                    /* If we found checker on our route, maybe we can jump. */
                    /* We need to check if color is good and if it is first checker on ther route*/
                    if ((copy = fields[posX + i + 8 * (posY + i)].getChecker()) != null
                            && (copy.getColor() == color || jumped != null)) {
                        break;
                    } else if (copy != null) {
                        jumped = copy;
                    }
                } // if(posX + i < 8 && posY + i < 8)
                else {
                    break;
                }
            }// for  
        }// if(x > posX && y > posY)
        else if (x > posX && y < posY) {
            for (int i = 1; i < 8; ++i) {
                /* Up right */
                if (posX + i < 8 && posY - i >= 0) {
                    if (x == posX + i && y == posY - i) {
                        if (jumped == null) {
                            incrementKingsMoves(color);
                            return processHumanMove(ch, pos);
                        } // if(jumped == null)
                        else {
                            deleteChecker(jumped);
                            return processHumanJump(ch, pos);
                        }// else

                    } // if(x == posX + i && y == posY - i)
                    if ((copy = fields[posX + i + 8 * (posY - i)].getChecker()) != null
                            && (copy.getColor() == color || jumped != null)) {
                        break;
                    } else if (copy != null) {
                        jumped = copy;
                    }
                } // if(posX + i < 8 && posY - i >= 0)
                else {
                    break;
                }
            }// for  
        }// else if(x > posX && y < posY) 
        else if (x < posX && y > posY) {
            for (int i = 1; i < 8; ++i) {
                /* Down left*/
                if (posX - i >= 0 && posY + i < 8) {
                    if (x == posX - i && y == posY + i) { // Znaleziono ruch
                        if (jumped == null) {
                            incrementKingsMoves(color);
                            return processHumanMove(ch, pos);
                        } // if(jumped == null)
                        else {
                            deleteChecker(jumped);
                            return processHumanJump(ch, pos);
                        }// else

                    } // if(x == posX - i && y == posY + i)
                    if ((copy = fields[posX - i + 8 * (posY + i)].getChecker()) != null
                            && (copy.getColor() == color || jumped != null)) {
                        break;
                    } else if (copy != null) {
                        jumped = copy;
                    }
                } // if(posX - i >= 0 && posY + i < 8)
                else {
                    break;
                }
            }// for  
        }// else if(x < posX && y > posY)
        else { //if(x < posX && y < posY){ 
            for (int i = 1; i < 8; ++i) {
                /* Up left */
                if (posX - i >= 0 && posY - i >= 0) {
                    if (x == posX - i && y == posY - i) { // Znaleziono ruch
                        if (jumped == null) {
                            incrementKingsMoves(color);
                            return processHumanMove(ch, pos);
                        } // if(jumped == null)
                        else {
                            deleteChecker(jumped);
                            return processHumanJump(ch, pos);
                        }// else

                    } // if(x == posX - i && y == posY - i)
                    if ((copy = fields[posX - i + 8 * (posY - i)].getChecker()) != null
                            && (copy.getColor() == color || jumped != null)) {
                        break;
                    } else if (copy != null) {
                        jumped = copy;
                    }
                } // if(posX - i >= 0 && posY - i >= 0)
                else {
                    break;
                }
            }// for  
        }// else
        return MoveState.INCORRECT;
    }

    /**
     * Checks if given color has jump available
     *
     * @param color color to check for jumps
     * @return true if jump found, else false
     */
    boolean hasJump(CheckerColor color) {
        switch (color) {
            case WHITE:
                for (Iterator<WhiteChecker> it = whiteCheckers.iterator(); it.hasNext();) {
                    Checker i = it.next();
                    if (hasJump(i)) {
                        return true;
                    }
                }
                break;
            default:
                for (Iterator<BlackChecker> it = blackCheckers.iterator(); it.hasNext();) {
                    Checker i = it.next();
                    if (hasJump(i)) {
                        return true;
                    }
                }
        }
        return false;

    }

    /**
     * Checks if given color has normal move or jump available
     *
     * @param color color to check for moves
     * @return true if move found, else false
     */
    boolean hasMove(CheckerColor color) {
        switch (color) {
            case WHITE:
                for (Iterator<WhiteChecker> it = whiteCheckers.iterator(); it.hasNext();) {
                    Checker i = it.next();
                    if (hasMove(i)) {
                        return true;
                    }
                }
                break;
            default:
                for (Iterator<BlackChecker> it = blackCheckers.iterator(); it.hasNext();) {
                    Checker i = it.next();
                    if (hasMove(i)) {
                        return true;
                    }
                }
        }
        return false;
    }

    /**
     * Checks if given checker has move available
     *
     * @param i checker to check for move
     * @return true if move found, else false
     */
    boolean hasMove(Checker i) {
        int x = i.getPositionX();
        int y = i.getPositionY();
        CheckerColor color = i.getColor();

        switch (i.getType()) {
            case NORMAL:
                /* If checker is normal */
                if (color == BLACK) {
                    /* if black */
                    if ((x + 1 <= 7) && (y - 1 >= 0) && fields[(x + 1) + 8 * (y - 1)].getChecker() == null) {
                        return true;
                    }
                    if ((x - 1 >= 0) && (y - 1 >= 0) && fields[(x - 1) + 8 * (y - 1)].getChecker() == null) {
                        return true;
                    }
                } // if(color == WHITE)
                else {
                    /* if white */
                    if ((x + 1 <= 7) && (y + 1 <= 7) && fields[(x + 1) + 8 * (y + 1)].getChecker() == null) {
                        return true;
                    }
                    if ((x - 1 >= 0) && (y + 1 <= 7) && fields[(x - 1) + 8 * (y + 1)].getChecker() == null) {
                        return true;
                    }
                } // else
                break;

            case KING:
                /* if checker is a king */
                boolean a, /* Four booleans which can block each diagonal checking */
                 b,
                 c,
                 d;
                a = b = c = d = true;
                /* Iterate through diagonal */
                for (int k = 1; k < 8; k++) {
                    if (a == true && (x + k <= 7) && (y + k <= 7)) {
                        /* Check down right */
                        if (fields[(x + k) + 8 * (y + k)].getChecker() != null) {
                            a = false; // Checker in the line
                        } else {
                            return true;
                        }
                    } // if (a == true && (x + k < 7) && (y + k < 7)
                    if (b == true && (x + k <= 7) && (y - k >= 0)) {
                        /* Check up right */
                        if (fields[(x + k) + 8 * (y - k)].getChecker() != null) {
                            /* if found found checker dont look futher */
                            b = false;
                        } else {
                            return true;
                        }
                    } // if(b == true && (x + k < 7) && (y - k > 0))
                    if (c == true && (x - k >= 0) && (y + k <= 7)) {
                        /* Check down left */
                        if (fields[(x - k) + 8 * (y + k)].getChecker() != null) {
                            c = false;
                        } else {
                            return true;
                        }
                    } // if(c == true && (x - k > 0) && (y + k < 7))
                    if (d == true && (x - k >= 0) && (y - k >= 0)) {
                        /* Check up left */
                        if (fields[(x - k) + 8 * (y - k)].getChecker() != null) {
                            d = false;
                        } else {
                            return true;
                        }
                    } // if(d == true && (x - k > 0 ) && (y - k > 0)
                } // for
        }
        return hasJump(i); /* Check checker for jumps */
    }

    /**
     * Checks if given checker has jump available
     *
     * @param i checker to check
     * @return true if jump found, else false
     */
    boolean hasJump(Checker i) {
        int x = i.getPositionX();
        int y = i.getPositionY();
        CheckerColor color = i.getColor();
        Checker copy;
        switch (i.getType()) {
            case NORMAL:
                /* Standard checking direction like in other methods */
                if ((x + 2 <= 7) && (y + 2 <= 7) && fields[(x + 2) + 8 * (y + 2)].getChecker() == null
                        && (copy = fields[x + 1 + 8 * (y + 1)].getChecker()) != null && copy.getColor() == color.opposite()) {
                    return true;
                }
                if ((x + 2 <= 7) && (y - 2 >= 0) && fields[(x + 2) + 8 * (y - 2)].getChecker() == null
                        && (copy = fields[x + 1 + 8 * (y - 1)].getChecker()) != null && copy.getColor() == color.opposite()) {
                    return true;
                }
                if ((x - 2 >= 0) && (y + 2 <= 7) && fields[(x - 2) + 8 * (y + 2)].getChecker() == null
                        && (copy = fields[x - 1 + 8 * (y + 1)].getChecker()) != null && copy.getColor() == color.opposite()) {
                    return true;
                }
                if ((x - 2 >= 0) && (y - 2 >= 0) && fields[(x - 2) + 8 * (y - 2)].getChecker() == null
                        && (copy = fields[x - 1 + 8 * (y - 1)].getChecker()) != null && copy.getColor() == color.opposite()) {
                    return true;
                }
                break; // case NORMAL
            case KING:
                /**
                 * Similar to hasMove method, although when it found a checker
                 * on diagonal line it checks if we can jump ater him
                 */
                boolean a = true,
                 b = true,
                 c = true,
                 d = true;
                for (int k = 1; k < 8; k++) {
                    if (a == true && (x + k < 7) && (y + k < 7)) {
                        if ((copy = fields[(x + k) + 8 * (y + k)].getChecker()) != null && copy.getColor() == color) {
                            a = false;
                        } else if (copy != null && fields[x + (k + 1) + 8 * (y + (k + 1))].getChecker() != null) {
                            a = false;
                        } // else if
                        else if (copy != null) {
                            return true;
                        }
                    } // if (a == true && (x + k < 7) && (y + k < 7)
                    if (b == true && (x + k < 7) && (y - k > 0)) {
                        if ((copy = fields[(x + k) + 8 * (y - k)].getChecker()) != null && copy.getColor() == color) {
                            b = false;
                        } else if (copy != null && fields[x + (k + 1) + 8 * (y - (k + 1))].getChecker() != null) {
                            b = false;
                        } // else if
                        else if (copy != null) {
                            return true;
                        }
                    } // if(b == true && (x + k < 7) && (y - k > 0))
                    if (c == true && (x - k > 0) && (y + k < 7)) {
                        if ((copy = fields[(x - k) + 8 * (y + k)].getChecker()) != null && copy.getColor() == color) {
                            c = false;
                        } else if (copy != null && fields[x - (k + 1) + 8 * (y + (k + 1))].getChecker() != null) {
                            c = false;
                        } // else if
                        else if (copy != null) {
                            return true;
                        }
                    } // if(c == true && (x - k > 0) && (y + k < 7))
                    if (d == true && (x - k > 0) && (y - k > 0)) {
                        if ((copy = fields[(x - k) + 8 * (y - k)].getChecker()) != null && copy.getColor() == color) {
                            d = false;
                        } else if (copy != null && fields[x - (k + 1) + 8 * (y - (k + 1))].getChecker() != null) {
                            d = false;
                        } // else if
                        else if (copy != null) {
                            return true;
                        }
                    } // if(d == true && (x - k > 0 ) && (y - k > 0)
                } // for

        } // switch
        /* 404 Not found :) */
        return false;
    }

    /**
     * Find all jumps for given color and saves it in moves vector Similar to
     * hasJump method
     *
     * @param color color which we check for jumps
     * @param moves move vector for storin found jumps
     * @return true if jump found, otherwise false
     * @see #hasJump(model.CheckerColor)
     */
    boolean findJumps(CheckerColor color, ArrayList<Move> moves) {
        boolean ret = false;

        switch (color) {
            case WHITE:
                for (Iterator<WhiteChecker> it = whiteCheckers.iterator(); it.hasNext();) {
                    Checker i = it.next();
                    if (findJumps(i, moves, 1)) {
                        ret = true;
                    }

                }
                break;
            default:
                for (Iterator<BlackChecker> it = blackCheckers.iterator(); it.hasNext();) {
                    Checker i = it.next();
                    if (findJumps(i, moves, 1)) {
                        ret = true;
                    }
                }
        }
        return ret;
    }

    /**
     * Find all jumps for given checker and saves it in moves vector Similar to
     * hasJump method
     *
     * @param i reference to checker
     * @param moves move vector for storing found jumps
	 * @param jumps 
     * @return true if jump found, otherwise false
     * @see #hasJump(model.Checker)
     */
    boolean findJumps(Checker i, ArrayList<Move> moves, int jumps) {
        boolean ret = false;
        int x = i.getPositionX();
        int y = i.getPositionY();
        CheckerColor color = i.getColor();
        Checker copy;
        switch (i.getType()) {
            case NORMAL:
                if ((x + 2 <= 7) && (y + 2 <= 7) && fields[(x + 2) + 8 * (y + 2)].getChecker() == null
                        && (copy = fields[x + 1 + 8 * (y + 1)].getChecker()) != null && copy.getColor() == color.opposite()) {
                    ret = true;
                    makeJump(moves, color, CheckerType.NORMAL, i.getPositionOnBoard(), x + 1 + 8 * (y + 1), (x + 2) + 8 * (y + 2), jumps);
                }
                if ((x + 2 <= 7) && (y - 2 >= 0) && fields[(x + 2) + 8 * (y - 2)].getChecker() == null
                        && (copy = fields[x + 1 + 8 * (y - 1)].getChecker()) != null && copy.getColor() == color.opposite()) {
                    ret = true;
                    makeJump(moves, color, CheckerType.NORMAL, i.getPositionOnBoard(), x + 1 + 8 * (y - 1), (x + 2) + 8 * (y - 2), jumps);
                }
                if ((x - 2 >= 0) && (y + 2 <= 7) && fields[(x - 2) + 8 * (y + 2)].getChecker() == null
                        && (copy = fields[x - 1 + 8 * (y + 1)].getChecker()) != null && copy.getColor() == color.opposite()) {
                    ret = true;
                    makeJump(moves, color, CheckerType.NORMAL, i.getPositionOnBoard(), x - 1 + 8 * (y + 1), (x - 2) + 8 * (y + 2), jumps);
                }
                if ((x - 2 >= 0) && (y - 2 >= 0) && fields[(x - 2) + 8 * (y - 2)].getChecker() == null
                        && (copy = fields[x - 1 + 8 * (y - 1)].getChecker()) != null && copy.getColor() == color.opposite()) {
                    ret = true;
                    makeJump(moves, color, CheckerType.NORMAL, i.getPositionOnBoard(), x - 1 + 8 * (y - 1), (x - 2) + 8 * (y - 2), jumps);
                }
                break; // case NORMAL
            case KING:
                /* booleans which can block diagonal for further checking */
                boolean a = true,
                 b = true,
                 c = true,
                 d = true;
                /* We iterate through diagonals */
                for (int k = 1; k < 8; k++) {
                    /* We check each direction in standard order */
                    if (a == true && (x + k < 7) && (y + k < 7)) {
                        if ((copy = fields[(x + k) + 8 * (y + k)].getChecker()) != null && copy.getColor() == color) {
                            /* we found our checker on line */
                            a = false;
                        } else if (copy != null && fields[x + (k + 1) + 8 * (y + (k + 1))].getChecker() != null) {
                            /* no place after checker that we wanted can jumped over */
                            a = false;
                        } // else if
                        else if (copy != null) {
                            /* Success*/
                            ret = true;
                            for (int j = 1; j < 7; j++) {
                                /* Check rest of line(found jump included) */
                                if ((x + k + j < 8) && (y + k + j < 8) && fields[(x + k + j) + 8 * (y + k + j)].getChecker() == null) {
                                    /* save jump to the vector */
                                    makeJump(moves, color, CheckerType.KING, i.getPositionOnBoard(), x + k + 8 * (y + k), (x + k + j) + 8 * (y + k + j), jumps);
                                } else {
                                    /* we found other checker on line -> we cannot move futher */
                                    break;
                                }
                            }
                            a = false;
                        }
                    } // if (a == true && (x + k < 7) && (y + k < 7)

                    /* For comments look above */
                    if (b == true && (x + k < 7) && (y - k > 0)) {
                        if ((copy = fields[(x + k) + 8 * (y - k)].getChecker()) != null && copy.getColor() == color) {
                            b = false;
                        } else if (copy != null && fields[x + (k + 1) + 8 * (y - (k + 1))].getChecker() != null) {
                            b = false;
                        } // else if
                        else if (copy != null) {
                            ret = true;
                            for (int j = 1; j < 7; j++) {
                                if ((x + k + j < 8) && (y - k - j > -1) && fields[(x + k + j) + 8 * (y - k - j)].getChecker() == null) {
                                    makeJump(moves, color, CheckerType.KING, i.getPositionOnBoard(), x + k + 8 * (y - k), (x + k + j) + 8 * (y - k - j), jumps);
                                } else {
                                    break;
                                }
                            }
                            b = false;
                        }
                    } // if(b == true && (x + k < 7) && (y - k > 0))
                    if (c == true && (x - k > 0) && (y + k < 7)) {
                        if ((copy = fields[(x - k) + 8 * (y + k)].getChecker()) != null && copy.getColor() == color) {
                            c = false;
                        } else if (copy != null && fields[x - (k + 1) + 8 * (y + (k + 1))].getChecker() != null) {
                            c = false;
                        } // else if
                        else if (copy != null) {
                            ret = true;
                            for (int j = 1; j < 7; j++) {
                                if ((x - k - j > -1) && (y + k + j < 8) && fields[(x - k - j) + 8 * (y + k + j)].getChecker() == null) {
                                    makeJump(moves, color, CheckerType.KING, i.getPositionOnBoard(), x - k + 8 * (y + k), (x - k - j) + 8 * (y + k + j), jumps);
                                } else {
                                    break;
                                }
                            }
                            c = false;
                        }
                    } // if(c == true && (x - k > 0) && (y + k < 7))
                    if (d == true && (x - k > 0) && (y - k > 0)) {
                        if ((copy = fields[(x - k) + 8 * (y - k)].getChecker()) != null && copy.getColor() == color) {
                            d = false;
                        } else if (copy != null && fields[x - (k + 1) + 8 * (y - (k + 1))].getChecker() != null) {
                            d = false;
                        } // else if
                        else if (copy != null) {
                            ret = true;
                            for (int j = 1; j < 7; j++) {
                                if ((x - k - j > -1) && (y - k - j > -1) && fields[(x - k - j) + 8 * (y - k - j)].getChecker() == null) {
                                    makeJump(moves, color, CheckerType.KING, i.getPositionOnBoard(), x - k + 8 * (y - k), (x - k - j) + 8 * (y - k - j), jumps);
                                } else {
                                    break;
                                }
                            }
                            d = false;
                        }
                    } // if(d == true && (x - k > 0 ) && (y - k > 0)
                } // for
        } // switch
        return ret;
    }

    /**
     * Saves given checker move to vector on proper place
     *
     * @param moves Move ArrayList for found moves
     * @param color color of the checker that jumps
     * @param type type of the checker that jumps
     * @param start start field of the checker that jumps
     * @param jumped field of checker that has been jumped over
     * @param end end field of the checker that jumps
     * @param jumps number of jumps
     */
    private void makeJump(ArrayList<Move> moves, CheckerColor color, CheckerType type, int start, int jumped, int end, int jumps) {
        /* copy of the checker */
        Checker copy;
        /* multijump flag */
        boolean multi = false;
        /* New BoardModel initialization */
        BoardModel boardCopy = new BoardModel(this, color.opposite());

        /* We look through moves array to find out if given jump is multijump or not */
        /* We start searching from the end which protects us from taking not our multijump part */
        for (int j = moves.size() - 1; j >= 0; j--) {
            Move i = moves.get(j);
            if (i.end == start && i.jumps == jumps - 1) {
                /* We found a multi jump */
                i.toDel = true; /* Set old part to delete as not final jump */
                /* Make new move from old one */
                Move m = new Move(i.start, end, i.boards.get(0), i.jumps + 1);
                /* Iterate through boards */
                Iterator<BoardModel> it = i.boards.iterator();
                it.next(); /* Ignore first board, cause we copied it when making Move */
                for (; it.hasNext();) {
                    /* Assigning boards */
                    BoardModel bm = it.next();
                    m.boards.add(bm);
                }
                /* Final board adding */
                m.boards.add(boardCopy);
                moves.add(m);
                multi = true;
                break;
            }
        }
        /* if not multi jump */
        if (multi == false) {
            moves.add(new Move(start, end, boardCopy, 1));
        }

        /* Update gameboard */
        if (color == WHITE) {
            boardCopy.fields[end].setChecker(copy = new WhiteChecker(end, type));
            boardCopy.whiteCheckers.remove((WhiteChecker) fields[start].getChecker());
            boardCopy.blackCheckers.remove((BlackChecker) fields[jumped].getChecker());
            boardCopy.whiteCheckers.add((WhiteChecker) copy);
        } else {
            boardCopy.fields[end].setChecker(copy = new BlackChecker(end, type));
            boardCopy.blackCheckers.remove((BlackChecker) fields[start].getChecker());
            boardCopy.whiteCheckers.remove((WhiteChecker) fields[jumped].getChecker());
            boardCopy.blackCheckers.add((BlackChecker) copy);
        }
        boardCopy.fields[start].setChecker(null);
        boardCopy.fields[jumped].setChecker(null);
        if (boardCopy.findJumps(copy, moves, jumps + 1) == false && type == CheckerType.NORMAL) {
            /* Check if multijump */
            copy.promote();
        }
    }

    /**
     * Find all normal moves for given color and saves it in moves vector
     * Similar to hasMove method
     *
     * @param color color which we check for normal moves
     * @param moves move vector for storing found normal moves
     * @return true if move was found, otherwise false
     * @see #hasMove(model.CheckerColor)
     */
    boolean findNormalMoves(CheckerColor color, ArrayList<Move> moves) {
        boolean ret = false;

        switch (color) {
            case WHITE:
                for (Iterator<WhiteChecker> it = whiteCheckers.iterator(); it.hasNext();) {
                    Checker i = it.next();
                    if (findNormalMoves(i, moves)) {
                        ret = true;
                    }
                }
                break;
            default:
                for (Iterator<BlackChecker> it = blackCheckers.iterator(); it.hasNext();) {
                    Checker i = it.next();
                    if (findNormalMoves(i, moves)) {
                        ret = true;
                    }
                }
                break;
        }
        return ret;
    }

    /**
     * Find all normal moves for given color and saves it in moves vector
     * Similar to hasMove method
     *
     * @param i checker ot check for normal moves
     * @param moves move vector for storing found normal moves
     * @return true if move was found, otherwise false
     * @see #hasMove(model.Checker)
     */
    boolean findNormalMoves(Checker i, ArrayList<Move> moves) {
        /* Checke hasMove(Model.Checker) for additional info */
        boolean ret = false;
        int x = i.getPositionX();
        int y = i.getPositionY();
        int pos = i.getPositionOnBoard();
        CheckerColor color = i.getColor();

        switch (i.getType()) {
            case NORMAL:
                if (color == BLACK) {
                    if ((x + 1 <= 7) && (y - 1 >= 0) && fields[(x + 1) + 8 * (y - 1)].getChecker() == null) {
                        ret = true;
                        /* Founded move so make it real!*/
                        makeMove(moves, color, CheckerType.NORMAL, pos, x + 1 + 8 * (y - 1));
                    }
                    if ((x - 1 >= 0) && (y - 1 >= 0) && fields[(x - 1) + 8 * (y - 1)].getChecker() == null) {
                        ret = true;
                        makeMove(moves, color, CheckerType.NORMAL, pos, x - 1 + 8 * (y - 1));
                    }

                } // if(color == WHITE)
                else {
                    if ((x + 1 <= 7) && (y + 1 <= 7) && fields[(x + 1) + 8 * (y + 1)].getChecker() == null) {
                        ret = true;
                        makeMove(moves, color, CheckerType.NORMAL, pos, x + 1 + 8 * (y + 1));
                    }
                    if ((x - 1 >= 0) && (y + 1 <= 7) && fields[(x - 1) + 8 * (y + 1)].getChecker() == null) {
                        ret = true;
                        makeMove(moves, color, CheckerType.NORMAL, pos, x - 1 + 8 * (y + 1));
                    }
                } // else
                break;
            case KING:
                boolean a = true,
                 b = true,
                 c = true,
                 d = true;
                for (int k = 1; k < 8; k++) {
                    if (a == true && (x + k <= 7) && (y + k <= 7)) {
                        if (fields[(x + k) + 8 * (y + k)].getChecker() != null) {
                            a = false; // Checker in the line
                        } else {
                            ret = true;
                            makeMove(moves, color, CheckerType.KING, pos, (x + k) + 8 * (y + k));
                        }
                    } // if (a == true && (x + k < 7) && (y + k < 7)
                    if (b == true && (x + k <= 7) && (y - k >= 0)) {
                        if (fields[(x + k) + 8 * (y - k)].getChecker() != null) {
                            b = false;
                        } else {
                            ret = true;
                            makeMove(moves, color, CheckerType.KING, pos, (x + k) + 8 * (y - k));
                        }
                    } // if(b == true && (x + k < 7) && (y - k > 0))
                    if (c == true && (x - k >= 0) && (y + k <= 7)) {
                        if (fields[(x - k) + 8 * (y + k)].getChecker() != null) {
                            c = false;
                        } else {
                            ret = true;
                            makeMove(moves, color, CheckerType.KING, pos, (x - k) + 8 * (y + k));
                        }
                    } // if(c == true && (x - k > 0) && (y + k < 7))
                    if (d == true && (x - k >= 0) && (y - k >= 0)) {
                        if (fields[(x - k) + 8 * (y - k)].getChecker() != null) {
                            d = false;
                        } else {
                            ret = true;
                            makeMove(moves, color, CheckerType.KING, pos, (x - k) + 8 * (y - k));
                        }
                    } // if(d == true && (x - k > 0 ) && (y - k > 0)
                } // for
        }
        return ret;
    }

    /**
     * Updates board and saves saves move to vector
     *
     * @param moves vector, which stores moves
     * @param color color of checker, which moves
     * @param type type of checker, which moves
     * @param start start field of the move
     * @param end end field of the move
     */
    void makeMove(ArrayList<Move> moves, CheckerColor color, CheckerType type, int start, int end) {
        Checker temp;
        BoardModel boardCopy = new BoardModel(this, color.opposite());
        //copy(boardCopy);
        /* Create move */
        moves.add(new Move(start, end, boardCopy));

        /* Update board */
        if (color == BLACK) {
            boardCopy.fields[end].setChecker(temp = new BlackChecker(end, type));
            boardCopy.blackCheckers.remove((BlackChecker) fields[start].getChecker());
            boardCopy.blackCheckers.add((BlackChecker) temp);
        } else {
            boardCopy.fields[end].setChecker(temp = new WhiteChecker(end, type));
            boardCopy.whiteCheckers.add((WhiteChecker) temp);
            boardCopy.whiteCheckers.remove((WhiteChecker) fields[start].getChecker());
        }
        boardCopy.fields[start].setChecker(null);
        /* Check if we can promote */
        temp.promote();

    }

    /**
     * Finds the best move for computer using minimax alghoritm
     *
     * @param color color of the computer
     * @param limit limit of move searching
     * @return best move for the computer
     */
    public Move cpuMove(CheckerColor color, int limit) //Ruch gracza
    {   /* New vector for moves */
        ArrayList<Move> moves = new ArrayList<>();

        BoardModel bestBoard = null;
        Move bestMove = null;
        /* Find moves for computer */
        AI.setColor(color);
        AI.findMoves(moves, this);  //Wyszukiwanie mozliwych ruchow
        System.out.println("Moves nr: " + moves.size());
        if (moves.isEmpty()) {
            if (color == BLACK) {
                players[0].setVictorious(true);
            } else {
                players[1].setVictorious(true);
            }
            return null;
        }
        /* Initial value of move value */
        int val = -AI.INFINITY;
        /* depth of the searching */
        int[] depth = {0};
        /* initial value of min depth */
        int shortestDepth = AI.INFINITY;
        /* Get value for each move */
        for (Iterator<Move> it = moves.iterator(); it.hasNext();) {
            Move i = it.next();

            int tmp1; /* Holds temporary value */
            /* Holds temporary BoardModel object */
            BoardModel tmp2 = i.boards.get(i.boards.size() - 1);
            if ((tmp1 = AI.newSearch(color, tmp2, limit, depth)) > val) {
                /* Better move has been found, take it */
                val = tmp1;
                bestMove = i;
                bestBoard = tmp2;
                shortestDepth = depth[0];
            } else if (tmp1 == val) {
                if (depth[0] < shortestDepth) {
                    /* Take move with lower depth */
                    val = tmp1;
                    bestMove = i;
                    bestBoard = tmp2;
                    shortestDepth = depth[0];
                } else if (depth[0] == shortestDepth) {
                    /* Or if depth is equal choose move at random */
                    Random randomGenerator = new Random();
                    if (randomGenerator.nextInt(2) == 0) {
                        val = tmp1;
                        bestMove = i;
                        bestBoard = tmp2;
                    }
                }
            }
        }
        /* Blocked */
        if (bestBoard == null || bestMove == null) {
            return null;
        }

        Checker ch = bestBoard.fields[bestMove.end].getChecker();
//        ch.promote();
        /* Handles king moves counting */
        if (bestMove.jumps > 0) {
            bestBoard.resetKingsMovesCounter();
        } else if (ch.getType() == CheckerType.KING) {
            if (color == BLACK) {
                bestBoard.blackKingsMoves++;
            } else {
                bestBoard.whiteKingsMoves++;
            }
        }

        System.out.println("Value: " + val + "  Depth: " + shortestDepth);
        System.out.println("Move from: " + bestMove.start + " to: " + bestMove.end + ", jumps: " + bestMove.jumps);

        return bestMove;
    }

    /* class for testing, not included in JavaDoc 
    public void test() {

        for (int i = 0; i < 64; i++) {
            fields[i].setChecker(null); //Kopiowanie ruchu do orginalnej planszy
        }
        fields[35].setChecker(new BlackChecker(35, CheckerType.NORMAL));
        fields[39].setChecker(new BlackChecker(39, CheckerType.NORMAL));
        fields[28].setChecker(new WhiteChecker(28, CheckerType.NORMAL));
        fields[14].setChecker(new WhiteChecker(14, CheckerType.NORMAL));
        fields[12].setChecker(new WhiteChecker(12, CheckerType.NORMAL));
        fields[10].setChecker(new WhiteChecker(10, CheckerType.NORMAL));
        fields[30].setChecker(new WhiteChecker(30, CheckerType.NORMAL));


        whiteCheckers.clear();
        blackCheckers.clear();
        for (int i = 0; i < 64; i++) {
            if (fields[i].getChecker() != null) {
                if (fields[i].getChecker() instanceof WhiteChecker) {
                    whiteCheckers.add((WhiteChecker) fields[i].getChecker());
                } else {
                    blackCheckers.add((BlackChecker) fields[i].getChecker());
                }
            }
        }

    }*/
}
