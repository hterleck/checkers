package model;

import java.io.Serializable;

/**
 * Checker on gameboard class
 *
 * @author Hubert Terlecki
 */
public abstract class Checker implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Checker position x(columns) on the board
     */
    private int positionX;
    /**
     * Checker position y(rows) on the board
     */
    private int positionY;
    /* Checker color used cause its quicker than instanceof */
    private final CheckerColor color;
    /* Type of checker */
    private CheckerType type;

    /**
     * Make new Checker
     *
     * @param color color of checker sended from subclass
     */
    Checker(CheckerColor color) {

        this.color = color;
        type = CheckerType.NORMAL;

        positionX = -1;
        positionY = -1;
    }

    /**
     * Create checker from data
     *
     * @param pos number of field on the board
     * @param type type of checker
     * @param color checker color
     */
    Checker(int pos, CheckerType type, CheckerColor color) {
        this.color = color;
        this.type = type;

        positionX = pos % 8;
        positionY = pos / 8;
    }

    /**
     * Copying constructor
     *
     * @param ch checker which we copy
     */
    Checker(Checker ch) {
        positionX = ch.positionX;
        positionY = ch.positionY;
        color = ch.color;
        type = ch.type;
    }

    /**
     * Gets checker type
     *
     * @return type of the checker
     */
    public CheckerType getType() {
        return type;
    }

    /**
     * Srts position of checker
     *
     * @param x position x (columns)
     * @param y position y (rows)
     */
    public void setPositionOnBoard(int x, int y) {
        positionX = x;
        positionY = y;
    }

    /**
     * Gets filed number on which checker stays
     *
     * @return position on board
     */
    public int getPositionOnBoard() {
        return positionY * 8 + positionX;
    }

    /**
     * Gets position x (columns)
     *
     * @return position x
     */
    public int getPositionX() {
        return positionX;
    }

    /**
     * Gets position y (rows)
     *
     * @return position y
     */
    public int getPositionY() {
        return positionY;
    }
    /**
     * Promotes normal checker to king
     */
    public void promote() { 
        if (color == CheckerColor.WHITE && positionY == 7) {
            type = CheckerType.KING;
        } else if (color == CheckerColor.BLACK && positionY == 0) {
            type = CheckerType.KING;
        }
    }

    /**
     * Gets checker color
     *
     * @return color of checker
     */
    public CheckerColor getColor() {
        return color;
    }

    /**
     * Gets value of checker for AI evaluation
     *
     * @return value of checker
     */
    public int getValue() {
        if (color == CheckerColor.WHITE) {
            if (type == CheckerType.NORMAL) {
                return -100;
            } else {
                return -400;
            }
        } else {
            if (type == CheckerType.NORMAL) {
                return 100;
            } else {
                return 400;
            }
        }
    }
}
