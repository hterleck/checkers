/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Color of the checker enum
 *
 * @author Hubert Terlecki
 */
public enum CheckerColor {
    /** White chekcer color */
    WHITE,
    /** Black checker color */
    BLACK;
    
    /**
     * Inverts color
     * @return inverted color
     */
    public CheckerColor opposite() {
        return this == CheckerColor.WHITE ? BLACK : WHITE;
    }
}