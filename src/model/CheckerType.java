/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hubert Terlecki
 */
public enum CheckerType {

    /** Normal checker, can move just one field atonce */
    NORMAL,
    /** King checker, can move more than one field at once*/
    KING,
}
