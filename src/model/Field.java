package model;

/**
 *  Field on the board which can contain checker
 *
 * @author Hubert Terlecki
 */
public class Field {
    //private boolean isOccupied;

    private Checker checker;

    Field() {
        checker = null;
    }
    /**
     * Returns checker which stays on field
     * @return checker on given field, or null if field is empty
     */
    public Checker getChecker() {
        return checker;
    }
    /**
     * Sets field to new checker
     * @param checker new checker on field reference
     */
    public void setChecker(Checker checker) {
        this.checker = checker;
    }
}
