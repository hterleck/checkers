package model;

/**
 * Type of game enum
 *
 * @author Hubert Terlecki
 */
public enum GameMode {
    
    /** Single player mode */
    SINGLEPLAYER,
    /** Hot seat mode*/
    LOCAL_MULTIPLAYER,
    /** Internet multiplayer mode */ 
    INTERNET_MULTIPLAYER,
}
