package model;

import java.util.ArrayList;

/**
 * Checker finall Move on the board withcontaining moves boards if jump
 *
 * @author Hubert Terlecki
 */
public class Move {

    /**
     * Starting field of the checker
     */
    int start;
    /**
     * Ending field of the checker
     */
    int end;
    /**
     * Move jumps counter
     */
    int jumps;
    /* Not end move, flagged to deleted */
    boolean toDel = false;
    /* List of parts of final move */
    ArrayList<BoardModel> boards;

    /**
     * Constructor
     *
     * @param start Starting field of the checker
     * @param end Ending field of the checker
     * @param tab Gameboard after move
     */
    Move(int start, int end, BoardModel tab) {
        /* Assigning values */
        this.start = start;
        this.end = end;
        /* Creates ArrayList and puts into it BoardModel */
        boards = new ArrayList<>();
        boards.add(tab);
    }

    /**
     *
     * @param start Starting field of the checker
     * @param end Ending field of the checker
     * @param tab Gameboard after move
     * @param jumps Number of jumps in this move
     */
    Move(int start, int end, BoardModel tab, int jumps) {
        this(start, end, tab);
        this.jumps = jumps;
    }

    /**
     * Gets baord states list
     *
     * @return ArrayList with succeeding moves(not final)
     */
    public ArrayList<BoardModel> getBoards() {
        return boards;
    }
}
