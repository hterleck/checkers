/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Possible user wanted move states
 *
 * @author Hubert Terlecki
 */
public enum MoveState {
    
    /** Correct move */
    CORRECT,
    /** Move is correct, but u have jump and u must proceed it */
    DOJUMP,
    /** Incorrect move */
    INCORRECT,
    /** After jum u have another jump */
    MULTIJUMP;
}
