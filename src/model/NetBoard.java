package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class used for sending game information through network
 *
 * @author Szymon Andrzejuk
 */
public class NetBoard implements Serializable {
    private static final long serialVersionUID = 1L;
	/** holds information about all black checkers on board */
    private ArrayList<BlackChecker> blackCheckers;
	/** holds information about all black checkers on board */
    private ArrayList<WhiteChecker> whiteCheckers;
	/** whose turn it is */
    private CheckerColor actualMoveColor;
	/** number of moves made using white kings without any effect */
    private int whiteKingsMoves = 0;
	/** number of moves made using black kings without any effect */
    private int blackKingsMoves = 0;
	/** locks the board if it's not your turn */
    private boolean onlineLock;

	/** 
	 * Class constructor. Makes exact copy of objects to send through network to other player
	 * 
	 * @param board reference to board model
	 */
    public NetBoard(final BoardModel board) {
        blackCheckers = new ArrayList<>();
        whiteCheckers = new ArrayList<>();
        for (Iterator<WhiteChecker> it = board.getWhiteCheckersArray().iterator(); it.hasNext();) {
            WhiteChecker i = it.next();
            WhiteChecker n = new WhiteChecker(i);
            whiteCheckers.add(n);
        }
        for (Iterator<BlackChecker> it = board.getBlackCheckersArray().iterator(); it.hasNext();) {
            BlackChecker i = it.next();
            BlackChecker n = new BlackChecker(i);
            blackCheckers.add(n);
        }
        actualMoveColor = board.getActualColor();
        whiteKingsMoves = board.getWhiteKingsMoves();
        blackKingsMoves = board.getBlackKingsMoves();
        onlineLock = board.getOnlineLock();
    }

	/**
	 * Gets actual checker color
	 * 
	 * @return actual move checker color
	 */
    public CheckerColor getCheckerColor() {
        return actualMoveColor;
    }

	/**
	 * Gets number of white kings moves without any effect
	 * 
	 * @return number of moves made with white kings without an effect
	 */
    public int getWhiteKingsMoves() {
        return whiteKingsMoves;
    }

	/**
	 * Gets number of black kings moves without any effect
	 * 
	 * @return number of moves made with white kings without an effect
	 */
    public int getBlackKingsMoves() {
        return whiteKingsMoves;
    }

	/**
	 * Gets online lock on local machine
	 * 
	 * @return true - lock is on, false - lock is off
	 */
    public boolean getOnlineLock() {
        return onlineLock;
    }

	/**
	 * Gets white checkers array list
	 * 
	 * @return reference to white checkers array list
	 */
    public ArrayList<WhiteChecker> getWhiteCheckersArray() {
        return whiteCheckers;
    }

	/**
	 * Gets black checkers array list
	 * 
	 * @return reference to black checkers array list
	 */
    public ArrayList<BlackChecker> getBlackCheckersArray() {
        return blackCheckers;
    }
}

