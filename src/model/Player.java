package model;

/**
 * Stores information about player
 *
 * @author Szymon Andrzejuk
 */
public class Player {
	/** player's name */
    private String playerName;
	/** flag that tells who won the game. false - player 1 won, true - player 2 won */
    private boolean isVictorious;

	/** 
	 * Class constructor
	 */
    public Player() {
		
    }

	/**
	 * Class constructor
	 * 
	 * @param name player's name
	 */
    public Player(String name) {
        playerName = name.trim();
        isVictorious = false;
    }

	/** 
	 * Gets player's name
	 * 
	 * @return player's name
	 */
    public String getName() {
        return playerName;
    }

	/**
	 * Sets player name
	 * @param name player's name
	 */
    public void setName(String name) {
        playerName = name;
    }

	/**
	 * Sets victory flag for a player
	 * 
	 * @param vic true - is victorious
	 */
    public void setVictorious(boolean vic) {
        isVictorious = vic;
    }

    /**
	 * Check's if player is victorious
	 * 
	 * @return true - player victorious
	 */
	public boolean isVictorious() {
        return isVictorious;
    }
}
