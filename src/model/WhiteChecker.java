package model;

/**
 * White checker class
 *
 * @author Hubert Terlecki
 */
public class WhiteChecker extends Checker {

    /**
     * WhiteChecker deafult constructor
     */
    public WhiteChecker() {
        super(CheckerColor.WHITE);
    }

    /**
     * WhiteChecker constructor
     *
     * @param pos position on board
     * @param type type of checker
     */
    public WhiteChecker(int pos, CheckerType type) {
        super(pos, type, CheckerColor.WHITE);
    }

    /**
     * WhiteChecker constructor
     *
     * @param ch reference to checker
     */
    public WhiteChecker(Checker ch) {
        super(ch);
    }
}
