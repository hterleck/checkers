package network;

import controller.BoardControllerOnline;
import model.NetBoard;
import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Network client class
 *
 * Class reads events from server and execute proper actions which handle this
 * events
 *
 *
 * @author Hubert Terlecki
 */
public class Client implements Runnable {

    /**
     * client socket
     */
    public Socket socket;
    /**
     * Client Stream to send events
     */
    private ObjectOutputStream oout;
    /**
     * Client stream to receive events
     */
    private ObjectInputStream oin;
    /**
     * Reference to boardControllerOnline object
     */
    private BoardControllerOnline bco;
    /**
     * Client thread reference
     */
    private Thread thread;
    /**
     * Client ask port
     */
    private int port;
    /**
     * Host address
     */
    private String host;
    /**
     * Map connecting event and actions
     */
    private final Map<Class<? extends OnlineEvent>, Action> onlineEventToAction = new HashMap<>();
    
    /** 
     * Sets board controller to given
     * 
     * @param board new board controller reference
     */
    public void setBoardControllerOnline(BoardControllerOnline board) {
        bco = board;
    }

    /**
     * Super class for events classe
     */
    abstract private class Action {

        /**
         * Make his own actions
         */
        abstract void execute(OnlineEvent event);
    }

    /**
     * Updates gameboard basing on received NetBoardEvent
     */
    private class NetBoardAction extends Action {

        @Override
        void execute(final OnlineEvent onlineEvent) {
            System.out.println("net");
            NetBoardEvent netBoardEvent = (NetBoardEvent) onlineEvent;
            NetBoard netBoard = netBoardEvent.getNetBoard();
            bco.updateBoard(netBoard);
        }
    }

    /**
     * Shows received messeges in chat window.
     */
    private class MessageAction extends Action {

        @Override
        void execute(final OnlineEvent onlineEvent) {
            System.out.println("mes");
            MessageEvent messageEvent = (MessageEvent) onlineEvent;
            String message = messageEvent.getMessage();
            bco.getChatController().updateChat(message);
        }
    }

    /**
     * Shows prompt and closes connection/server
     */
    private class PlayerLeftGameAction extends Action {

        @Override
        void execute(final OnlineEvent onlineEvent) {
            System.out.println("left");
            stop();
            bco.lostConnection();
        }
    }

    /**
     * Shows end of game dialog
     */
    private class EndBoxAction extends Action {

        @Override
        void execute(final OnlineEvent onlineEvent) {
            System.out.println("endbox");
            if (bco != null) {
                bco.showEndGameStats();
            }
        }
    }

    /**
     * Shows prompt and closes connection/server
     */
    private class RestartGameAction extends Action {

        @Override
        void execute(final OnlineEvent onlineEvent) {
            System.out.println("res");
            try {
                if (bco.getBoardModel() == null) {
                    stop();
                }
                if (bco.isServer()) {
                    bco.getChatController().updateChat("Client wants to restart the game");
                } else {
                    bco.getChatController().updateChat("Server wants to restart the game");
                    bco.getChatController().updateChat("Game restarted");
                    processEvent(new MessageEvent("Game restarted"));
                    bco.getBoardModel().setOnlineLock(true);
                    processEvent(new RestartGameEvent.DeactivateOnlineLockEvent());
                }
            } catch (NullPointerException e) {
                stop();
                /* Sb wants restart, sb quit */
            }
        }
    }

    /**
     * Shows prompt and closes connection/server
     */
    private class DeactivateOnlineLockAction extends Action {

        @Override
        void execute(final OnlineEvent onlineEvent) {
            System.out.println("deac");
            bco.getBoardModel().setOnlineLock(false);
        }
    }

    /**
     * Sends information about the start with name of the player Proper name is
     * saved in boadModel
     */
    private class StartOfGameAction extends Action {

        @Override
        void execute(final OnlineEvent onlineEvent) {
            System.out.println("start");
            StartOfGameEvent event = (StartOfGameEvent) onlineEvent;
            String name = event.getName();
            /* If we created the server */
            if (bco.getBoardModel() == null) {
                return;
            }
            if (bco.getBoardModel().getPlayer(1).getName().equals("")) {
                bco.updateName(name, 1);
                bco.getChatController().updateChat("Info: " + name + " connected.");
                bco.getChatController().updateChat("Info: " + bco.getBoardModel().getPlayer(0).getName() + ", please start the game.");
                processEvent(new StartOfGameEvent(bco.getBoardModel().getPlayer(0).getName()));
            } /* If we are just client */ else {
                bco.updateName(name, 0);
                bco.getChatController().updateChat("Info: Connected to server.");
                bco.getChatController().updateChat("Info: " + bco.getBoardModel().getPlayer(0).getName() + " starts the game.");
            }
        }
    }

    /**
     * Stops connection
     */
    private class EndOfGameAction extends Action {

        @Override
        void execute(final OnlineEvent onlineEvent) {
            System.out.println("EOG");
            stop();
        }
    }

    /**
     *
     *
     * @param host server address
     * @param port connection port
     * @param bco reference to BoardControllerOnline
     */
    public Client(String host, int port, BoardControllerOnline bco) {
        /* Adding acions and events to map */
        onlineEventToAction.put(MessageEvent.class, new MessageAction());
        onlineEventToAction.put(NetBoardEvent.class, new NetBoardAction());
        onlineEventToAction.put(PlayerLeftGameEvent.class, new PlayerLeftGameAction());
        onlineEventToAction.put(StartOfGameEvent.class, new StartOfGameAction());
        onlineEventToAction.put(EndOfGameEvent.class, new EndOfGameAction());
        onlineEventToAction.put(RestartGameEvent.class, new RestartGameAction());
        onlineEventToAction.put(RestartGameEvent.DeactivateOnlineLockEvent.class, new DeactivateOnlineLockAction());
        onlineEventToAction.put(EndBoxEvent.class, new EndBoxAction());
        /* Saving references */
        this.bco = bco;
        this.host = host;
        this.port = port;
    }

    /**
     * Gets called when user sends some event
     * 
     * @param event one of OnlineEvents, which will be sent
     */
    public void processEvent(OnlineEvent event) {
        try {
            // Send it to the server
            oout.writeObject(event);
            oout.flush();
            System.out.println("Event has been sent.");

        } catch (IOException e) {
            System.out.println("Server off");
        }
    }

    /** Backkground thread proceed given event */
    @Override
    public void run() {
        // Receive messages one-by-one, forever
        OnlineEvent event = null;
        while (thread != null) {
            // Get the next message
            try {

                event = (OnlineEvent) oin.readObject();
            } catch (IOException e) {
                if (bco.isServer() == false) {
                    PlayerLeftGameAction action = new PlayerLeftGameAction();
                    action.execute(null);
                } else {
                    break;
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                break;
            }
            // Print it to our text window
            System.out.println("Received online event");
            Class<?> eventClass = event.getClass();

            /* Make action */
            Action gameAction = onlineEventToAction.get(eventClass);

            gameAction.execute(event);

        }

        try {
            oin.close();
            oout.close();
            socket.close();
        } catch (IOException e) {
            System.out.println("Connection already closed.");
        }
    }

    /**
     * Stops connection and ends loop in client thread
     */
    public void stop() {
        thread = null;
    }

    /**
     * starts client thread and making connection
     *
     * @throws IOException when server is unreachable
     */
    public void start() throws IOException {
        /* Quick server check */
        socket = new Socket();
        socket.connect(new InetSocketAddress(host, 5000), 3500);

        System.out.println("Connected to " + socket);

        /* Create streams */
        oin = new ObjectInputStream(socket.getInputStream());
        oout = new ObjectOutputStream(socket.getOutputStream());
        /* Start thread */
        (thread = new Thread(this)).start();
    }
}
