package network;

/**
 * Chat message event class
 *
 * @author Hubert Terlecki
 */
public class MessageEvent extends OnlineEvent {

    /**
     * Holds message
     */
    public String message;

    /**
     * Constructor
     *
     * @param message which we want to send
     */
    public MessageEvent(String message) {
        this.message = message;
    }

    /**
     * Returns message
     *
     * @return received message
     */
    public String getMessage() {
        return message;
    }
}
