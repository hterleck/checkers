package network;

import model.NetBoard;

/**
 * Event class used to send a gameboard
 *
 * @author Hubert Terlecki
 */
public class NetBoardEvent extends OnlineEvent{
    /** Copy of gameboard */
    private final NetBoard nb;
 
    /**
     * Reference assigning constructor
     * 
     * @param nb 
     */
    
    public NetBoardEvent(final NetBoard nb) {
        this.nb = nb;
    }
 
    /**
     * Returns gameboard
     * 
     * @return Copy of gameboard.
     */
    public NetBoard getNetBoard() {
        return nb;
    }
}
