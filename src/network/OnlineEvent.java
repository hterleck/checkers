
package network;

import java.io.Serializable;

/**
 * Abstract class representing events we catch during online games
 * 
 * @author Hubert Terlecki
 */
public abstract class OnlineEvent implements Serializable {
   static final long serialVersionUID = 1L;
 
}
