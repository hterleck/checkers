
package network;

/**
 * Event sent when restarting online game
 * 
 * @author Szymon Andrzejuk
 */
public class RestartGameEvent extends OnlineEvent {
	
	/**
	* Deactivating board lock on server end when game is restarted
	*
	* @author Szymon Andrzejuk
	*/
	public static class DeactivateOnlineLockEvent extends OnlineEvent {
		
	}
	
}
