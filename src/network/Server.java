package network;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * Network Server class
 *
 * Class listens for clients and send events to them.
 *
 * @author Hubert Terlecki
 */
public class Server implements Runnable {

    /**
     * ServerSocket usd to waiting for clients
     */
    private ServerSocket ss;
    /**
     * Reference to server theard
     */
    private Thread thread;
    /**
     * Hashtable holding information about clients threads
     */
    private Hashtable<ServerThread, ObjectOutputStream> serverThreads = new Hashtable<>();

    /**
     * Constructor
     */
    public Server() {
        this.thread = new Thread(this);
    }

    /**
     * Listining for clients to join server
     *
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        System.out.println("Listening on " + ss);

        while (thread != null) {
            try {
                /* Grab incoming connection */
                Socket socket = ss.accept();
                ObjectOutputStream oout = new ObjectOutputStream(socket.getOutputStream());
                ServerThread serverThread = new ServerThread(this, socket, oout);
                serverThread.start();


                System.out.println("Connection from " + socket);

                if (serverThreads.isEmpty()) {
                    serverThreads.put(serverThread, oout);
                } else {
                    /* Second Player */
                    serverThreads.put(serverThread, oout);
                    thread = null;
                }


            } catch (IOException e) {
                System.out.println("Listening closed.");
            }
        }
        // Close server socket
        try {
            ss.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Enumeration for hashtable
     *
     * @return ServerTheard - key of hashtable
     */
    Enumeration getServerThreads() {
        return serverThreads.keys();
    }

    /**
     * Send event to opponent
     *
     * @param event event to be sent
     * @param thread thread of player, who send event
     */
    void sendToOpponent(OnlineEvent event, ServerThread thread) {
        for (Enumeration e = getServerThreads(); e.hasMoreElements();) {
            ServerThread st = (ServerThread) e.nextElement();
            if (st.getSocket().isClosed()) {
                return;
            }
            if (st == thread) {
                continue;
            }
            try {
                ObjectOutputStream oout = serverThreads.get(st);
                oout.writeObject(event);
                oout.flush();
            } catch (IOException ie) {
                ie.printStackTrace();
            }
        }
    }

    /**
     * Send event to each user
     *
     * @param event event to be sent
     */
    void sendToAll(OnlineEvent event) {
        for (Enumeration e = getServerThreads(); e.hasMoreElements();) {
            ServerThread st = (ServerThread) e.nextElement();
            if (st.getSocket().isClosed()) {
                return;
            }
            try {
                ObjectOutputStream oout = serverThreads.get(st);
                oout.writeObject(event);
                oout.flush();
            } catch (IOException ie) {
                System.out.println("Connection already closed.");
            }
        }
    }

    /**
     * Starts server on port 5000
     *
     * @throws IOException when port is busy
     */
    public void start() throws IOException {
        ss = new ServerSocket(5000);
        thread.start();
    }

    /**
     * Stops clients conenction listener theard and other theards which handle clients
     * 
     * @throws IOException when I/O error occurs
     */
    public void stop() throws IOException {

        for (Enumeration e = getServerThreads(); e.hasMoreElements();) {
            ServerThread st = (ServerThread) e.nextElement();
            st.stop();
        }
        thread = null;
        ss.close();
    }
}
