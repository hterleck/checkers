
package network;

import java.io.*;
import java.net.*;

/**
 * Class for server thread which handles one client 
 * 
 * @author Hubert Terlecki
 */
public class ServerThread implements Runnable
{
	/** ServerThread thread reference */
	private Thread thread;
	/** Link to server object */
	private Server server;
	/** The Socket connected to our client */
	private Socket socket;
        
	/** Input stream from client */
	private final ObjectInputStream oin;
	/** Output Stream to client */
	private final ObjectOutputStream oout;

	/** Constructor 
	 * 
	 * @param server server object reference
	 * @param socket socket of the client
	 * @param oout output stream to client
	 * @throws IOException cannot create the input stream
	 */
	public ServerThread( Server server, Socket socket, ObjectOutputStream oout ) throws IOException {
		
                /* Copying references */
                this.server = server;
		this.socket = socket;
                this.oout = oout;
                /* Make thread and save its reference */
                this.thread = new Thread(this);
                /* Make new input stream for Client */
                this.oin = new ObjectInputStream(socket.getInputStream());
	}
	
	
	/** 
         * Waiting for input from client, which transfer to other clients by Server
         * 
         * @see java.lang.Runnable#run()
         */
	public void run() {
            /* Itereate till stop() call */
            while (thread != null) {
                try
                {
                    OnlineEvent onlineEvent = (OnlineEvent) oin.readObject();
                    /* End of game should be send to everyone */
                    if(onlineEvent instanceof EndOfGameEvent )
                        server.sendToAll(onlineEvent);
                    else
                        server.sendToOpponent(onlineEvent, this);
                }
                catch (IOException e)
                {
                    /* Player closed application or connection is broken */

                   server.sendToAll(new PlayerLeftGameEvent());
                    break;
                }
                /* Bad class has been send */
                catch (ClassNotFoundException e)
                {
                    e.printStackTrace();
                    break;
                }                    
            }
            /** We close streams and server socket */
            try
            {
                oin.close();
                oout.close();
                socket.close();
            }
            catch (IOException e)
            {
               System.out.println("Connection was closed before.");
            }
        }
        /**
         * Returns client socket
         * 
         * @return socket of client which is assigned to this serverThread
         */
        Socket getSocket(){
            return socket;
        }
        /** 
         * Starts reading and sending objects
         */
        void start() {
            thread.start();
        }
        /** 
         * Stop reading and sending objects
         */
        void stop(){
            thread = null;
        }
}
