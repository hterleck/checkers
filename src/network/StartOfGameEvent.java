
package network;

/**
 * Event sent when game starts, contains names of players
 * 
 * @author Hubert Terlecki
 */
public class StartOfGameEvent extends OnlineEvent {
        /** Name of player, who send event */
        public String name;
        
        /** Constructor 
         * 
         * @param name of player, who want to start the game
         */
	public StartOfGameEvent(String name) {
		this.name = name;		
	}
        /**
         * Returns player name
         * @return player nname
         */
        public String getName(){
            return name;
        }
}
