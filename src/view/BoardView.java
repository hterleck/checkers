package view;
import model.GameMode;
import model.WhiteChecker;
import model.CheckerColor;
import model.BoardModel;
import model.CheckerType;
import model.Checker;
import model.BlackChecker;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.image.BufferedImage;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import org.imgscalr.Scalr;

/**
 * Displays game board with checkers. Contains methods that allow controller to update the view. It overrides PaintComponent
 * function so it can draw game board and checkers.
 *
 * @author Szymon Andrzejuk
 */
public class BoardView extends MenuCore {
	/** positions of board fields on screen (in pixels) */
	private Point2D fieldPositionOnScreen[];
	/** board's field size (in pixels) */
	private Point2D fieldSize;
	/** board size (in pixels) */
	private Point2D boardSize;
	/** checker size (in pixels) */
    private Point2D checkerSize;
    /** board model */
	private BoardModel boardModel;
	/** currently dragged checker by user */
	private Checker draggedChecker;
	/** flag, which tells if any checker is being currently dragged on screen */
	private boolean dragged = false;
	/** flag, which tells if mouse button was pressed */
	private boolean mousePressed = false;
	/** flag, which tells if last current move is a multijump */
	private boolean multijump = false;
	/** position of dragged checker on screen */
	private Point checkerDraggedPositionOnScreen = new Point();
	/** initial position of dragged checker on board */
	private Point initialCheckerPositionOnBoard = new Point();
	/** board image's border width (in pixels) */
	private double borderWidth;
	/** board image's border height (in pixels) */
	private double borderHeight;
    /** "Back to menu" button */
	private JButton endGameButton;
	/** "Rules" button */
	private JButton gameRulesButton;
	/** chat text field. Available only in internet multipplayer */
	private JTextField chatTextField;
	/** chat text area. Available only in internet multipplayer */
	private JTextArea chatTextArea;
	/** displays game stats (current turn) */
	private JTextPane gameInfoArea;
	/** holds current turn player's checker */
	private ImageIcon currentTurn;
	/** width of panel containing information about the game, buttons and chat field */
	private int panelWidth;
	/** When game is initialized it signals paintComponent function to evaluate fields and checkers positions*/
	private boolean gameInitialization = true;

	/**
	 * Class constructor. Adds components to program's frame.
	 * 
	 * @param frame main frame of the program
	 * @param imagesLoaded graphic assets loaded by the program
	 * @param boardModel board model
	 * @see Frame
	 * @see ImagesLoader
	 * @see BoardModel
	 */
	public BoardView(Frame frame, ImagesLoader imagesLoaded, final BoardModel boardModel) {
		super(frame, imagesLoaded);
		this.boardModel = boardModel;
		draggedChecker = null;
		fieldPositionOnScreen = new Point2D[64];
		fieldSize = new Point2D.Float();
		boardSize = new Point2D.Float();
		checkerSize = new Point2D.Float();

		panelWidth = 300;
		mainFrame.setSize(mainFrame.getWidth() + panelWidth, mainFrame.getHeight());

		mainFrame.setLayout(new BorderLayout());
		JPanel infoBar = new JPanel();
		mainFrame.add(this, BorderLayout.CENTER);
		infoBar.setLayout(new BoxLayout(infoBar,  BoxLayout.Y_AXIS));

		JPanel buttonRow = new JPanel();
		JPanel chatPanel = new JPanel();
		JPanel gameStatsPanel = new JPanel();

		endGameButton = new JButton();
		gameRulesButton = new JButton();
		
		Color color = getBackground();
		
		/** Setting up game stats panel (game info, chat and buttons) */
		gameStatsPanel.setMinimumSize(new Dimension(300,200));
		gameStatsPanel.setPreferredSize(new Dimension(300,200));
		gameStatsPanel.setMaximumSize(new Dimension(300,200));
		gameInfoArea = new JTextPane();
		
		StyledDocument doc = gameInfoArea.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);
		
		/** set initial text in game info area */
		gameInfoArea.setText(boardModel.getPlayer(0).getName()+"'s turn\n\n");
		currentTurn = new ImageIcon();
		currentTurn.setImage(getGraphicsAssets().whiteCheckerImage);
		gameInfoArea.insertIcon(currentTurn);
		gameInfoArea.setEditable(false);
		gameInfoArea.setFont(new Font("Default", Font.BOLD, 14));
		gameInfoArea.setBackground(color);
		gameStatsPanel.add(gameInfoArea);

		infoBar.add(gameStatsPanel);
		infoBar.add(Box.createVerticalGlue());

		BorderLayout chatPanelLayout = new BorderLayout();
		chatPanel.setLayout(chatPanelLayout);
		chatPanelLayout.setVgap(10);
		chatTextArea = new JTextArea();
		chatTextArea.setEditable(false);
		Font font = new Font("Default", Font.PLAIN, 12);
		chatTextArea.setFont(font);
		chatTextArea.setLineWrap(true);
		chatPanel.add(chatTextArea, BorderLayout.CENTER);
		chatTextField = new JTextField();
		chatTextField.setFont(font);
		chatPanel.add(chatTextField, BorderLayout.SOUTH);

		chatPanel.setBorder(new EmptyBorder(10,10,10,10));
		
		JScrollPane scrollPane = new JScrollPane(chatTextArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		/** Auto scrolling ability in chat */
		scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {  
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {  
					e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
			}
		});
		chatPanel.add(scrollPane);

		/** disable chat in local game modes */
		if(boardModel.getGameMode() == GameMode.SINGLEPLAYER || boardModel.getGameMode() == GameMode.LOCAL_MULTIPLAYER) {
			scrollPane.setVisible(false);
			chatTextField.setVisible(false);
			chatTextArea.setVisible(false);
		}
		infoBar.add(chatPanel);
		infoBar.add(Box.createVerticalGlue());

		buttonRow.setLayout(new FlowLayout());
		endGameButton = new JButton("Back to menu");
		buttonRow.add(endGameButton); 
		gameRulesButton = new JButton("Rules");
		buttonRow.add(gameRulesButton);
		infoBar.add(buttonRow);

		mainFrame.add(infoBar, BorderLayout.EAST);

		/** calculates fields positions on screen */
		for(int i = 0; i < 64; ++i) {
			fieldPositionOnScreen[i] = new Point2D.Double();
		}    
	}
	
	/** 
	 * Sets board model reference
	 * 
	 * @param boardModel board model 
	 */
	public void setModel(BoardModel boardModel) {
		this.boardModel = boardModel;
	}
	
	/** 
	 * Gets reference to chat text field
	 * 
	 * @return reference to chat text field 
	 */
	public JTextField getChatTextField() {
		return chatTextField;
	}

	/** 
	 * Gets reference to chat text area
	 * 
	 * @return reference to chat text area 
	 */
	public JTextArea getChatTextArea() {
		return chatTextArea;
	}

	/** 
	 * Sets current turn player's checker icon 
	 * 
	 * @param img image of black/white checker
	 */
	public void setCurrentTurnIcon(BufferedImage img) {
			currentTurn.setImage(img);
	}

	/**
	 * Check if checker is currently being dragged on screen
	 * 
	 * @return true if checker is currently dragged, false if isn't
	 */
	public boolean isBeingDragged() {
		return dragged;
	}

	/**
	 * Sets dragged flag to specified value
	 * 
	 * @param dragged true - checker is being dragged
	 */
	public void setDragged(boolean dragged) {
		this.dragged = dragged;
	}

	/** 
	 * Gets reference to currently dragged checker on screen
	 * 
	 * @return reference to dragged checker
	 */
	public Checker getDraggedChecker() {
		return draggedChecker;
	}

	/** 
	 * Sets currently dragged checker
	 * 
	 * @param checker currently dragged checker
	 */
	public void setDraggedChecker(Checker checker) {
		draggedChecker = checker;
	}

	/**
	 * Checks if mouse button was presed
	 * 
	 * @return true - mouse button was pressed
	 */
	public boolean isMousePressed() {
		return mousePressed;
	}

	/**
	 * Sets mouse pressed flag
	 * 
	 * @param mousePressed true - mouse was pressed
	 */
	public void setMousePressed(boolean mousePressed) {
		this.mousePressed = mousePressed;
	}

	/**
	 * Gets value, if last move was a multijump
	 * 
	 * @return true - was a multijump
	 */
	public boolean isMultijump() {
		return multijump;
	}

	/**
	 * Sets multijump flag
	 * 
	 * @param multijump true - current move is a multijump
	 */
	public void setMultijump(boolean multijump) {
		this.multijump = multijump;
	}

	/** 
	 * Gets board size
	 * 
	 * @return board size (in pixels)
	 */
	public Point2D getBoardSize() {
		return boardSize;
	}
	
	/**
	 * Gets board image's border width
	 * 
	 * @return border width
	 */
	public double getBorderWidth() {
		return borderWidth;
	}
	
	/**
	 * Gets board image's border height
	 * 
	 * @return border height
	 */
	public double getBorderHeight() {
		return borderHeight;
	}
	
	/** 
	 * Sets board size
	 * 
	 * @param x board width
	 * @param y board height
	 */
	public void setBoardSize(double x, double y) {
		boardSize.setLocation(x, y);
	}

	/** 
	 * Gets field size
	 * 
	 * @return field size (in pixels)
	 */
	public Point2D getFieldSize() {
		return fieldSize;
	}

	/** 
	 * Gets checker size
	 * 
	 * @return checker size (in pixels)
	 */
	public Point2D getCheckerSize() {
		return checkerSize;
	}

	/** 
	 * Sets checker size
	 * 
	 * @param x checker width
	 * @param y checker height
	 */
	public void setCheckerSize(double x, double y) {
		boardSize.setLocation(x, y);
	} 

	/** 
	 * Gets dragged checker initial position on board
	 * 
	 * @return initial position of dragged checker on board
	 */
	public Point getInitialCheckerPositionOnBoard() {
		return initialCheckerPositionOnBoard;
	}

	/** Sets dragged checker initial position on board
	 * 
	 * @param x checker's horizontal position on board
	 * @param y checker's vertical position on board
	 */
	public void setInitialCheckerPositionOnBoard(int x, int y) {
		initialCheckerPositionOnBoard.setLocation(x, y);
	}

	/** 
	 * Gets dragged checker current position on screen
	 * 
	 * @return position on screen (in pixels)
	 */
	public Point2D getCheckerDraggedPositionOnScreen() {
		return checkerDraggedPositionOnScreen;
	}

	/**
	 * Sets dragged checker position on board
	 * 
	 * @param x checker's horizontal position on board
	 * @param y checker's vertical position on board
	 */
	public void setCheckerDraggedPositionOnBoard(int x, int y) {
		draggedChecker.setPositionOnBoard(x, y);
	}

	/**
	 * Sets dragged checker current position on screen
	 * 
	 * @param x checker's horizontal position on screen
	 * @param y checker's vertical position on screen
	 */
	public void setCheckerDraggedPositionOnScreen(double x, double y) {
		checkerDraggedPositionOnScreen.setLocation(x, y);
	}

	/**
	 * Adds listener for dragging checker event
	 * 
	 * @param checkerDraggedListener specified mouse motion listener
	 */
	public void addCheckerDraggedListener(MouseMotionListener checkerDraggedListener) {
		this.addMouseMotionListener(checkerDraggedListener);
	}

	/**
	 * Adds listener for releasing checker event
	 * 
	 * @param checkerReleasedListener specified mouse listener
	 */
	public void addCheckerReleasedListener(MouseListener checkerReleasedListener) {
		this.addMouseListener(checkerReleasedListener);
	}

	/**
	 * Adds listener for "Back to menu" button
	 * 
	 * @param endGameButtonPressed specified button listener
	 */
	public void addEndGameButtonListener(ActionListener endGameButtonPressed) {
		endGameButton.addActionListener(endGameButtonPressed);
	}

	/**
	 * Adds listener for "Rules" button
	 * 
	 * @param gameRulesButtonPressed specified button listener
	 */
	public void addGameRulesButtonListener(ActionListener gameRulesButtonPressed) {
		gameRulesButton.addActionListener(gameRulesButtonPressed);
	}

	/**
	 * Adds listener for chat field
	 * 
	 * @param ChatMessageTyped chat listener
	 */
	public void addChatListener(ActionListener ChatMessageTyped) {
		chatTextField.addActionListener(ChatMessageTyped);
	}

	/**
	 * Reloads information about the game (player's turn)
	 */
	public void reloadGameInfoArea() {
		CheckerColor turn = boardModel.getActualColor();
		int t = turn == CheckerColor.WHITE ? 0 : 1; 
		gameInfoArea.setText(boardModel.getPlayer(t).getName()+"'s turn\n\n");
		gameInfoArea.insertIcon(currentTurn);
	}
	
	/**
	 * Repaints game board with checkers
	 * 
	 * @param g Graphics object
	 */
    @Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		/* images resized on board initialization (if necessary) */
		if(graphicsAssets.scaledBackgroundMenuImage.getHeight() != graphicsAssets.scaledBackgroundBoardImage.getHeight()) {
			graphicsAssets.scaledBackgroundBoardImage = Scalr.resize(graphicsAssets.backgroundBoardImage, 
				Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 
				mainFrame.getContentPane().getWidth() - panelWidth, 
				mainFrame.getContentPane().getHeight(), Scalr.OP_ANTIALIAS);
			graphicsAssets.scaledBlackCheckerImage = Scalr.resize(graphicsAssets.blackCheckerImage, 
				Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 
				mainFrame.getContentPane().getHeight()/9, 
				mainFrame.getContentPane().getHeight()/9, Scalr.OP_ANTIALIAS);
			graphicsAssets.scaledWhiteCheckerImage = Scalr.resize(graphicsAssets.whiteCheckerImage, 
				Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 
				getHeight()/9, getHeight()/9, Scalr.OP_ANTIALIAS);
			graphicsAssets.scaledBlackCheckerKingImage = Scalr.resize(graphicsAssets.blackCheckerKingImage, 
				Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 
				mainFrame.getContentPane().getHeight()/9, 
				mainFrame.getContentPane().getHeight()/9, Scalr.OP_ANTIALIAS);
			graphicsAssets.scaledWhiteCheckerKingImage = Scalr.resize(graphicsAssets.whiteCheckerKingImage, 
				Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 
				getHeight()/9, getHeight()/9, Scalr.OP_ANTIALIAS);
		}
		/** calculating board and checker size based on current window size */
		if(gameInitialization) {
			boardSize.setLocation((mainFrame.getContentPane().getWidth()- panelWidth),
			   (mainFrame.getContentPane().getHeight()));
			int x = 0, y = 0;
			checkerSize.setLocation(graphicsAssets.scaledBlackCheckerImage.getWidth(), 
				graphicsAssets.scaledBlackCheckerImage.getHeight()); 
			/** Calculating each checker's position on field (where exalcty to draw checker's image) */
			borderWidth = (mainFrame.getContentPane().getWidth() - panelWidth) * 0.0104;
			borderHeight = (mainFrame.getContentPane().getHeight() * 0.0104);
			fieldSize.setLocation(((mainFrame.getContentPane().getWidth()- panelWidth - 2*borderWidth) / 8), 
				(mainFrame.getContentPane().getHeight() - 2*borderHeight) / 8);
			for(Point2D p : fieldPositionOnScreen) {
				p.setLocation(fieldSize.getX() * x + borderWidth + borderWidth * 0.5,
						   fieldSize.getY() * y+ borderHeight + borderHeight * 0.3);
				if(++x == 8) {
				   ++y;
				   x = 0;
				}
			}
			gameInitialization = false;
		}
		/** 
		 * Drawing board and checkers. Each time board and all checkers have to be redrawn so a dragged checker 
		 * doesn't leave marks when dragged
		 */
		g2d.drawImage(graphicsAssets.scaledBackgroundBoardImage, 0, 0, this);
		int index = 0;
		for(int i = 0; i < boardModel.getWhiteCheckersArray().size(); ++i) {
			WhiteChecker ch = boardModel.getWhiteCheckersArray().get(i);
			if((draggedChecker != ch) || multijump){
				index = ch.getPositionOnBoard();
				if(ch.getType() == CheckerType.NORMAL)
				    g2d.drawImage(graphicsAssets.scaledWhiteCheckerImage, (int)fieldPositionOnScreen[index].getX(), 
						(int)fieldPositionOnScreen[index].getY(), this);
				else
				    g2d.drawImage(graphicsAssets.scaledWhiteCheckerKingImage, (int)fieldPositionOnScreen[index].getX(), 
					    (int)fieldPositionOnScreen[index].getY(), this); 
			}
		}
		for(int i = 0; i < boardModel.getBlackCheckersArray().size(); ++i) {
			BlackChecker ch = boardModel.getBlackCheckersArray().get(i);
			if((draggedChecker != ch) || multijump) {
				index = ch.getPositionOnBoard();
				if(ch.getType() == CheckerType.NORMAL)
					g2d.drawImage(graphicsAssets.scaledBlackCheckerImage, (int)fieldPositionOnScreen[index].getX(), 
						(int)fieldPositionOnScreen[index].getY(), this);
				else
					g2d.drawImage(graphicsAssets.scaledBlackCheckerKingImage, (int)fieldPositionOnScreen[index].getX(), 
						(int)fieldPositionOnScreen[index].getY(), this);
			}
		}
		/** Drawing only dragged checker */
		if(dragged) {
			if(draggedChecker.getColor() == CheckerColor.BLACK) {
				if(draggedChecker.getType() == CheckerType.NORMAL)
					g2d.drawImage(graphicsAssets.scaledBlackCheckerImage, 
						checkerDraggedPositionOnScreen.x, checkerDraggedPositionOnScreen.y, this);
				else
					g2d.drawImage(graphicsAssets.scaledBlackCheckerKingImage, 
						checkerDraggedPositionOnScreen.x, checkerDraggedPositionOnScreen.y, this);
			}
			else {
				if(draggedChecker.getType() == CheckerType.NORMAL)
					g2d.drawImage(graphicsAssets.scaledWhiteCheckerImage, 
						checkerDraggedPositionOnScreen.x, checkerDraggedPositionOnScreen.y, this);
				else
					g2d.drawImage(graphicsAssets.scaledWhiteCheckerKingImage, 
						checkerDraggedPositionOnScreen.x, checkerDraggedPositionOnScreen.y, this);
			}
		}
	}

       
	/**
	 * Resets board view
	 */	
	public void reset() {
		draggedChecker = null;
		dragged = false;
		mousePressed = false;
		multijump = false;
	}
}