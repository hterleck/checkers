
package view;

import javax.swing.JFrame;
import model.CheckerColor;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * Contains various dialog boxes with different information
 *
 * @author Szymon Andrzejuk
 */
public class DialogBox {
	/**
	 * Displays dialog, which lets player to choose checker color in singleplayer
	 * 
	 * @param mainFrame reference to main frame
	 * @return selected checker color
	 */
	public static CheckerColor showCheckerColorBox(final Frame mainFrame) {
		String checker = "";
		String[] possibilities = {"Red          ", "White          "};
		checker = (String)JOptionPane.showInputDialog(
			mainFrame,
			"",
			"Choose checkers color",
			JOptionPane.INFORMATION_MESSAGE,
			null,
			possibilities,
			"");
		if(checker == null)
			return null;
		if(checker.equals("White          "))
			return CheckerColor.WHITE;
		else
			return CheckerColor.BLACK;
	}
	
	/**
	 * Displays dialog to insert player's name. 
	 * 
	 * @param i Specifies input mode: 
	 * @param mainFrame reference to main frame
	 * <p> i == 1 - Player 1 in hot seat mode
	 * <p> i == 2 - Player 2 in hot seat mode
	 * <p> i == 3 - Player 1 in multiplayer mode (create game)
	 * <p> i == 4 - Player 2 in multiplayer mode (join game)
	 * @return player's name inserted
	 */
	public static String showPlayerNameDialog(int i, final Frame mainFrame) {
		String name, message, player;
		if(i == 0) {
			message = "Enter Your name";
			player = "Player";
		}
		else if(i == 1) {
			message = "Enter white checkers player name";
			player = "Player 1";
		}
		else if(i == 2) {
			message = "Enter black checkers player name";
			player = "Player 2";
		}
		else if(i == 3){
			message = "Enter Your name";
			player = "Player 1";
		}
		else {
			message = "Enter Your name";
			player = "Player 2";
		}
		do {
			name = (String)JOptionPane.showInputDialog(
				mainFrame,
				"",
				message,
				JOptionPane.QUESTION_MESSAGE,
				null,
				null,
				player);
			System.out.println(name);
			if(name == null) 
				break;
			if(name.length() >= 21) {
				message = "Name too long!";
				name = "";
			}
		} while(name.equals(""));
		return name;
	}
	
	/**
	 * Displays game rules dialog
	 * 
	 * @param mainFrame reference to main frame
	 */	
	public static void showRulesDialog(final Frame mainFrame) {
		String rules = "1. Checkers is played by two players. Each player begins the game with 12 colored discs.\n"
						+ "2. The board consists of 64 squares, alternating between 32 dark and 32 light squares. It is \n" 
						+ "    positioned so that each player has a light square on the right side corner closest to him or her.\n" 
						+ "3.  White moves first. Players then alternate moves.\n" 
						+ "4. Moves are allowed only on the dark squares, so pieces always move diagonally. Single \n" 
						+ "    pieces are always limited to forward moves (toward the opponent).\n" 
						+ "5. A piece making a non-capturing move (not involving a jump) may move only one square.\n" 
						+ "6. A piece making a capturing move (a jump) leaps over one of the opponent's pieces, landing in a \n" 
						+ "    straight diagonal line on the other side. Only one piece may be captured in a single jump. \n" 
						+ "    However, multiple jumps are allowed on a single turn.\n" 
						+ "7. When a piece is captured, it is removed from the board.\n" 
						+ "8. If a player is able to make a capture, there is no option -- the jump must be made. If more than \n" 
						+ "    one capture is available, the player is free to choose whichever he or she prefers.\n" 
						+ "9. When a piece reaches the furthest row from the player who controls that piece, it is crowned\n" 
						+ "    and becomes a king. \n"
						+ "10. Kings are limited to moving diagonally, but may move both forward and backward.\n"
						+ "11. A player wins the game when the opponent cannot make a move. In most cases, this is\n"
						+ "      because all of the opponent's pieces have been captured, but it could also be because all\n"
						+ "      of his  pieces are blocked in. Draw result occurs when each player moves a king 15\n"
						+ "	     times without a capture.";
		JOptionPane.showMessageDialog(
			mainFrame,
			rules,
			"Game rules",
			JOptionPane.INFORMATION_MESSAGE);
	}
		
	/**
	 * Displays dialog, when user wants to edn the game dialog
	 * 
	 * @param mainFrame reference to main frame
	 * @return number of button clicked on dialog
	 */	
	public static int showEndGameDialog(final Frame mainFrame) {
		int n = JOptionPane.showConfirmDialog(
			mainFrame,
			"Do You want to end the game?",
			"",
			JOptionPane.YES_NO_OPTION);
		return n;
	}
	
	/**
	 * Displays end game statistics
	 * 
	 * @param winner victorious player's name
	 * @param duration duration of the game in miliseconds
	 * @param mainFrame reference to main frame
	 * @return number of button clicked on dialog
	 */
	public  static int showEndGameStats(String winner, long duration, Frame mainFrame) {
		Object[] options = {"Back to menu", "Restart game"};
		/** game duration in minutes */
		duration /= 60000;
		++duration;
		if(winner != null)
			winner += " is the winner!\nGame duration: "+duration+" minutes";
		else
			winner = "It's a draw!\nGame Duration: " + duration + " minutes";
		int n = JOptionPane.showOptionDialog(
			mainFrame,
			winner,
			"Game results",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null,
			options,
			options[1]);
		return n;
	}
	
	/**
	 * Displays dialog which gives various information to player
	 * 
	 * @param title dialog box title
	 * @param text text to display in a dialog
	 * @param mainFrame reference to main frame
	 */
	public static void showInformationWindow(final String title, final String text, final Frame mainFrame){
		Object[] options = {"Back to menu"};
		JOptionPane.showOptionDialog(
			mainFrame,
			text,
			title,
			JOptionPane.YES_NO_OPTION,
			JOptionPane.INFORMATION_MESSAGE,
			null,
			options,
			options[0]);
	}
	
	/**
	 * Displays dialog to insert ip address 
	 * 
	 * @param mainFrame reference to main frame
	 * @return ip address inserted
	 */
	public static String showIPDialog(final Frame mainFrame) {
		String ip = "";
		String message;
		do {
			ip = (String)JOptionPane.showInputDialog(
				mainFrame,
				"",
				"IP address",
				JOptionPane.QUESTION_MESSAGE,
				null,
				null,
				"127.0.0.1");
			System.out.println(ip);
			if(ip == null) break;
		} while(ip.equals(""));
		return ip;
	}	
	
	/** 
	 * Displays dialog which tells player if a jump is possible
	 * 
	 * @param mainFrame reference to main frame
	 */
	public static void jumpPossibleDialog(final Frame mainFrame) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JOptionPane.showMessageDialog(mainFrame, "Jump possible. Choose correct checker");
			}
		});
	}	
	
	/**
	 * Displays dialog when program can't load graphic's assets
	 * 
	 * @param mainFrame reference to main frame
	 */	
	public static void showErrorLoadingImagesDialog(final JFrame mainFrame) {
		JOptionPane.showMessageDialog(
			mainFrame,
			"Program loading failed. Exiting.",
			"Error",
			JOptionPane.ERROR_MESSAGE);
	}
}
