package view;


import controller.MainMenuController;
import java.awt.*;
import java.io.IOException;
import javax.swing.*;

/**
 * Main frame of the program 
 * 
 * @author Szymon Andrzejuk
 */
public class Frame extends JFrame {
	/** Default size of program's frame (screen height / 2) */
    private final int defaultSize;
	/** reference to graphic's assets loaded by the program */
	private ImagesLoader loadedImages;
	/** program's main frame */
	JFrame frame;
    
	/** 
	 * Gets frame's default size 
	 * 
	 * @return frame's default size
	 */
    public int getDefaultSize() {
        return defaultSize;
    }

    /**
	 * Class constructor
	 */
    public Frame() {
        super("Checkers");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
		setResizable(false);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/view/red.png")));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		defaultSize = (int)(screenSize.getHeight() / 2);
        Dimension initialSize = new Dimension(defaultSize, defaultSize);
        setLocation((int)screenSize.getHeight()/10, (int)screenSize.getWidth()/10);
		try {
			loadedImages = new ImagesLoader();
		} 
		catch (IllegalArgumentException | IOException ex) {
			DialogBox.showErrorLoadingImagesDialog(new JFrame());
			System.exit(0);
		}
		MainMenuView mainMenuView = new MainMenuView(this, loadedImages);
		MainMenuController c = new MainMenuController(mainMenuView);
        add(mainMenuView);
        pack();
		setSize(initialSize);
        setVisible(true); 
	}

	/** 
	 * Main function of the program. Creates new frame
	 * 
	 * @param args arguments passed to program. Not used
	 */
    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                    new Frame();
            }
        });
    }
}
