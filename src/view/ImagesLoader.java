/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Contains references to all graphic's assets used by program
 * 
 * @author Szymon Andrzejuk
 */
public class ImagesLoader {
	/** reference to backgroung menu image */
	public BufferedImage backgroundMenuImage;
	/** reference to backgroung board image */
    public BufferedImage backgroundBoardImage;
	/** reference to scaled backgroung board image */
	public BufferedImage scaledBackgroundBoardImage;
	/** reference to scaled backgroung menu image */
	public BufferedImage scaledBackgroundMenuImage;  
	/** reference to black checker image */
    public BufferedImage blackCheckerImage;
	/** reference to scaled black checker image */
    public BufferedImage scaledBlackCheckerImage;
	/** reference to black checker king image */
	public BufferedImage blackCheckerKingImage;
	/** reference to scaled black checker king image */
	public BufferedImage scaledBlackCheckerKingImage;
	/** reference to white checker image */
    public BufferedImage whiteCheckerImage;
	/** reference to scaled white checker image */
    public BufferedImage scaledWhiteCheckerImage;
	/** reference to white checker king image */
	public BufferedImage whiteCheckerKingImage;
	/** reference to white scaled checker king image */
	public BufferedImage scaledWhiteCheckerKingImage;

	/** Class constructor. Loads images used by program
	 * 
	 * @throws IOException failed reading images
	 */
	ImagesLoader() throws IOException, IllegalArgumentException {            
		backgroundMenuImage =  ImageIO.read(getClass().getResource("/view/checkers_menu1200.png"));
		scaledBackgroundMenuImage = backgroundMenuImage;
		backgroundBoardImage = ImageIO.read(getClass().getResource("/view/checkersBoard_1200.png"));
		scaledBackgroundBoardImage = backgroundBoardImage;
		blackCheckerImage = ImageIO.read(getClass().getResource("/view/red.png"));
		scaledBlackCheckerImage = blackCheckerImage;
		blackCheckerKingImage = ImageIO.read(getClass().getResource("/view/red_king.png"));
		scaledBlackCheckerKingImage = blackCheckerKingImage;
		whiteCheckerImage = ImageIO.read(getClass().getResource("/view/white.png"));
		scaledWhiteCheckerImage = whiteCheckerImage;
		whiteCheckerKingImage = ImageIO.read(getClass().getResource("/view/white_king.png"));
		scaledWhiteCheckerKingImage = whiteCheckerKingImage;
	}
}
