package view;
//import Controller.WindowResizeBoxListener;
import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;


/**
 * Main menu with game type selection (singleplayer/multiplayer)
 * 
 * @author Szymon Andrzejuk
 */
public class MainMenuView extends MenuCore {
	/** Single Player game menu button */
	private JButton singlePlayerButton;
	/** Multi Player game menu button */
	private JButton multiPlayerButton;
	/** Exit program button */
	private JButton exitButton;
	
	
	
	/**
	 * Class constructor g
	 * 
	 * @param frame main frame of the program
	 * @param loadedImages graphic assets loaded by the program
	 * @see Frame
	 * @see ImagesLoader
	 */
	public MainMenuView(Frame frame, ImagesLoader loadedImages) {
		super(frame, loadedImages);

		BoxLayout layout = new BoxLayout(this,  BoxLayout.Y_AXIS);
		setLayout(layout);

		singlePlayerButton = new JButton("Single Player");
		multiPlayerButton = new JButton("Multi Player");
		exitButton = new JButton("Exit");
		
		singlePlayerButton.setMaximumSize(new Dimension(150,25));
		multiPlayerButton.setMaximumSize(new Dimension(150,25));
		exitButton.setMaximumSize(new Dimension(150,25));
		
		singlePlayerButton.setPreferredSize(new Dimension(150,25));
		multiPlayerButton.setPreferredSize(new Dimension(150,25));
		exitButton.setPreferredSize(new Dimension(150,25));

		singlePlayerButton.setMinimumSize(new Dimension(150,25));
		multiPlayerButton.setMinimumSize(new Dimension(150,25));
		exitButton.setMinimumSize(new Dimension(150,25));
		
		singlePlayerButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		multiPlayerButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		exitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		add(new Box.Filler(new Dimension(3,3), new Dimension(10,10), new Dimension(20,20)));
		add(windowSizeBox);
		add(new Box.Filler(new Dimension(50,50), new Dimension(200,200), new Dimension(100000,100000)));
		
		add(Box.createVerticalGlue());
		add(singlePlayerButton);
		add(Box.createVerticalGlue());
		add(multiPlayerButton);
		add(Box.createVerticalGlue());
		add(exitButton);
		add(new Box.Filler(new Dimension(0,0), new Dimension(50,50), new Dimension(100,100)));
	}
	
	/**
	 * Adds "single player" button listener
	 * 
	 * @param singlePlayerButtonListener reference to action listener of specific component
	 */
	public void addSinglePlayerButtonListener(ActionListener singlePlayerButtonListener) {
		singlePlayerButton.addActionListener(singlePlayerButtonListener);
	}
	
	/**
	 * Adds "multi player" button listener
	 * 
	 * @param multiPlayerButtonListener reference to action listener of specific component
	 */
	public void addMultiPlayerButtonListener(ActionListener multiPlayerButtonListener) {
		multiPlayerButton.addActionListener(multiPlayerButtonListener);
	}
	
	/**
	 * Adds exit" button listener
	 * 
	 * @param exitButtonPressed reference to action listener of specific component
	 */
	public void addExitButtonListener(ActionListener exitButtonPressed) {
		exitButton.addActionListener(exitButtonPressed);
	}	
}