package view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import org.imgscalr.Scalr;


/**
 * Main frame for every window displayed in program
 * 
 * @author Szymon Andrzejuk
 */
public abstract class MenuCore extends JPanel {
	/** main frame of the program */
	protected Frame mainFrame;
	/** loaded image assets into the program */
	protected ImagesLoader graphicsAssets;
	/** tells the program whether it has to resize background image */
	private static boolean programInitialization = true;
	/** box providing ability to resize window size */
	protected JComboBox windowSizeBox;
	
	/**
	 * MenuCore constructor g
	 * 
	 * @param frame
	 * @param loadedImages 
	 * @see Frame
	 * @see ImagesLoader
	 */
	MenuCore(Frame frame, ImagesLoader loadedImages) {
        mainFrame = frame;
		graphicsAssets = loadedImages;
		String sizes[] = {"Window size:", "75%", "100%", "125%", "150%", "175%", "200%"};
		windowSizeBox = new JComboBox<>(sizes);
		windowSizeBox.setMaximumSize(new Dimension(150,25));
		windowSizeBox.setPreferredSize(new Dimension(150,25));
		windowSizeBox.setMinimumSize(new Dimension(150,25));
		windowSizeBox.setAlignmentX(Component.CENTER_ALIGNMENT);
	}
	
	/**
	 * Adds "screen size" combo box listener
	 * 
	 * @param windowResizeBoxListener action listener of specific component
	 */
	public void addWindowResizeBoxListener(ActionListener windowResizeBoxListener) {
		windowSizeBox.addActionListener(windowResizeBoxListener);
	}
	
	/**
	 * @return reference to "screen size" box component
	 */
	public JComboBox getWindowSizeBox() {
		return windowSizeBox;
	}

	/**
	 * @return reference to main frame of the program
	 */
	public Frame getFrame() {
		return mainFrame;
	}
     
	/**
	 * @return reference to graphics assets loaded to the program
	 */
	public ImagesLoader getGraphicsAssets() {
		return graphicsAssets;
	}
	 
	/** 
	 * Paints menu background an resizes background image if necessary
	 * 
	 * @param g Graphics object
	 */
	@Override
	public void paintComponent(Graphics g) {
		 Graphics2D g2d = (Graphics2D) g;
		 if(programInitialization == true) {
			 graphicsAssets.scaledBackgroundMenuImage = Scalr.resize(graphicsAssets.backgroundMenuImage, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 
				 mainFrame.getContentPane().getWidth(), mainFrame.getContentPane().getHeight(), Scalr.OP_ANTIALIAS);
			 programInitialization = false;
		 }
		 //System.out.println("repaint");
		 g2d.drawImage(graphicsAssets.scaledBackgroundMenuImage, 0, 0, this);
    }
}
