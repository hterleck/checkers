package view;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.plaf.LayerUI;


/**
 * Multi player menu with game type selection (hot seat, internet multiplayer)
 * 
 * @author Szymon Andrzejuk
 */
public class MultiPlayerMenuView extends MenuCore {
	/** Create hot seat game button */
	private JButton hotSeatGameButton;
	/** create network game button */
	private JButton createNetworkGameButton;
	/** Join network game button */
	private JButton joinNetworkGameButton;
	/** Go back to main menu button */
	private JButton backButton;
	
	/**
	 * Class constructor g
	 * 
	 * @param frame reference to main frame of the program
	 * @param loadedImages reference to graphic assets loaded by the program
	 * @see Frame
	 * @see ImagesLoader
	 */
	public MultiPlayerMenuView(Frame frame, ImagesLoader loadedImages) {
		super(frame, loadedImages);

		BoxLayout layout = new BoxLayout(this,  BoxLayout.Y_AXIS);
		setLayout(layout);
		
		hotSeatGameButton = new JButton("Hot seat");
		createNetworkGameButton = new JButton("Create game");
		joinNetworkGameButton = new JButton("Join game");
		backButton = new JButton("Back");

		hotSeatGameButton.setMaximumSize(new Dimension(150,25));
		createNetworkGameButton.setMaximumSize(new Dimension(150,25));
		joinNetworkGameButton.setMaximumSize(new Dimension(150,25));
		backButton.setMaximumSize(new Dimension(150,25));
		
		hotSeatGameButton.setPreferredSize(new Dimension(150,25));
		createNetworkGameButton.setPreferredSize(new Dimension(150,25));
		joinNetworkGameButton.setPreferredSize(new Dimension(150,25));
		backButton.setPreferredSize(new Dimension(150,25));
		
		hotSeatGameButton.setMinimumSize(new Dimension(150,25));
		createNetworkGameButton.setMinimumSize(new Dimension(150,25));
		joinNetworkGameButton.setMinimumSize(new Dimension(150,25));
		backButton.setMinimumSize(new Dimension(150,25));
		
		hotSeatGameButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		createNetworkGameButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		joinNetworkGameButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		add(new Box.Filler(new Dimension(3,3), new Dimension(10,10), new Dimension(20,20)));
		add(windowSizeBox);
		add(new Box.Filler(new Dimension(50,50), new Dimension(200,200), new Dimension(100000,100000)));
		
		add(Box.createVerticalGlue());
		add(hotSeatGameButton);
		add(Box.createVerticalGlue());
		add(createNetworkGameButton);
		add(Box.createVerticalGlue());
		add(joinNetworkGameButton);
		add(Box.createVerticalGlue());
		add(backButton);
		add(new Box.Filler(new Dimension(0,0), new Dimension(50,50), new Dimension(100,100)));
	}
	
	/**
	 * Adds "hot seat" button listener
	 * 
	 * @param hotSeatGameButtonListener reference to action listener of specific component
	 */
	public void addHotSeatButtonListener(ActionListener hotSeatGameButtonListener) {
		hotSeatGameButton.addActionListener(hotSeatGameButtonListener);
	}
	
	/**
	 * Adds "create game" button listener
	 * 
	 * @param createNetworkGameButtonListener reference to action listener of specific component
	 */
	public void addCreateGameButtonListener(ActionListener createNetworkGameButtonListener) {
		createNetworkGameButton.addActionListener(createNetworkGameButtonListener);
	}
	
	/**
	 * Adds "join game" button listener
	 * 
	 * @param joinGameButtonListener reference to action listener of specific component
	 */
	public void addJoinGameButtonListener(ActionListener joinGameButtonListener) {
		joinNetworkGameButton.addActionListener(joinGameButtonListener);
	}
	
	/**
	 * Adds "back" button listener
	 * 
	 * @param backButtonListener reference to action listener of specific component
	 */
	public void addBackButtonListener(ActionListener backButtonListener) {
		backButton.addActionListener(backButtonListener);
	} 
}
