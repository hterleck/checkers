package view;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 * Single player menu with game difficulty selection
 * 
 * @author Szymon Andrzejuk
 */
public class SinglePlayerMenuView extends MenuCore {
	/** Begin game button */
	private JButton beginGameButton;
	/** Back button */
	private JButton backButton;
	/** Choose difficulty combo box */
	private JComboBox<String> difficultyBox;

	
	/**
	 * Class constructor g
	 * 
	 * @param frame main frame of the program
	 * @param loadedImages graphic assets loaded by the program
	 * @see Frame
	 * @see ImagesLoader
	 */
	public SinglePlayerMenuView(Frame frame, ImagesLoader loadedImages) {
		super(frame, loadedImages);
		
		BoxLayout layout = new BoxLayout(this,  BoxLayout.Y_AXIS);
		setLayout(layout);
		
		String difficulties[] = {"Difficulty:", "Easy", "Medium", "Hard"};
		
		difficultyBox = new JComboBox<>(difficulties);
		backButton = new JButton("Back");
		beginGameButton = new JButton("Begin game");
		beginGameButton.setEnabled(false);
		
		difficultyBox.setAlignmentX(Component.CENTER_ALIGNMENT);
		beginGameButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		difficultyBox.setMaximumSize(new Dimension(150,25));
		beginGameButton.setMaximumSize(new Dimension(150,25));
		backButton.setMaximumSize(new Dimension(150,25));
		
		difficultyBox.setPreferredSize(new Dimension(150,25));
		beginGameButton.setPreferredSize(new Dimension(150,25));
		backButton.setPreferredSize(new Dimension(150,25));

		difficultyBox.setMinimumSize(new Dimension(150,25));
		beginGameButton.setMinimumSize(new Dimension(150,25));
		backButton.setMinimumSize(new Dimension(150,25));
		
		add(new Box.Filler(new Dimension(3,3), new Dimension(10,10), new Dimension(20,20)));
		add(windowSizeBox);
		add(new Box.Filler(new Dimension(50,50), new Dimension(200,200), new Dimension(100000,100000)));
		
		add(Box.createVerticalGlue());
		add(difficultyBox);
		add(Box.createVerticalGlue());
		add(beginGameButton);
		add(Box.createVerticalGlue());
		add(backButton);
		add(new Box.Filler(new Dimension(0,0), new Dimension(50,50), new Dimension(100,100)));
	}
	
	/** @return "begin game" button */
	public JButton getBeginGameButton() {
		return beginGameButton;
	}
	
	/** @return "choose difficulty" combo box */
	public JComboBox getDifficultyBox() {
		return difficultyBox;
	}
	
	/**
	 * Adds "begin game" button listener
	 * 
	 * @param beginGameButtonListener action listener of specified component
	 */
	public void addBeginGameButtonListener(ActionListener beginGameButtonListener) {
		beginGameButton.addActionListener(beginGameButtonListener);
	}
	
	/**
	 * Adds "choose difficulty" combo box listener
	 * 
	 * @param difficultyBoxListener action listener of specified component
	 */
	public void addDifficultyBoxListener(ActionListener difficultyBoxListener) {
		difficultyBox.addActionListener(difficultyBoxListener);
	}
	
	/**
	 * Adds "back" button listener
	 * 
	 * @param backButtonListener action listener of specified component
	 */
	public void addBackButtonListener(ActionListener backButtonListener) {
		backButton.addActionListener(backButtonListener);
	}	
}